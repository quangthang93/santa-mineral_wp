<?php get_header(); ?>

<main class="main p-end">
    <div class="container type2">
        <div class="breadcrumbWrap">
            <div class="breadcrumb">
                <?php wp_breadcrumb() ?>
            </div>
        </div><!-- ./breadcrumbWrap -->

        <section class="p-end--banner type2">
            <h1 class="p-end--ttl">
                <img src="<?php the_field('title_en'); ?>" alt="">
                <span><?php the_title() ?></span>
            </h1>
        </section><!-- ./p-recruit--banner -->

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; ?>
        <?php endif; ?>            
    </div>
    <div class="align-center mgt-60">
        <?php 
        if($locale == 'ja') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url(), 'トップページへ戻る');
        } elseif ($locale == 'en_US') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url(), 'Back to Top');
        } elseif ($locale == 'zh_CN') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url(), 'Back to Top');
        }
        ?>
    </div>
</main>

<?php get_footer(); ?>

<?php get_header(); ?>

<?php $queries_obj = get_queried_object();?>
<main class="main p-end">

    <div class="p-end--cnt">
        <div class="container">
            <div class="breadcrumbWrap">
                <div class="breadcrumb">
                    <ul>
                        <?php 
                        if($locale == 'ja') {
                            printf('<li><a href="%s">%s</a></li><li><a href="%s">%s</a></li>', home_url(), 'トップページ', home_url('service-product'), '技術・製品紹介');
                        } elseif ($locale == 'en_US') {
                            printf('<li><a href="%s">%s</a></li><li><a href="%s">%s</a></li>', home_url(), 'Top Page', home_url('service-product'), 'Technology / product introduction');
                        } elseif ($locale == 'zh_CN') {
                            printf('<li><a href="%s">%s</a></li><li><a href="%s">%s</a></li>', home_url(), 'Top Page', home_url('service-product'), '技術・產品介紹');
                        }
                        ?>
                        <li><?php echo $queries_obj->name?></li>
                    </ul>
                </div>
            </div><!-- ./breadcrumbWrap -->
            <section class="p-end--banner type2">
                <h1 class="p-end--ttl">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/end/ttl-service.png" alt="">
                    <?php 
                    if($locale == 'ja') {
                        printf("<span>技術・製品紹介</span>");
                    } elseif ($locale == 'en_US') {
                        printf("<span>Technology / product introduction</span>");
                    } elseif ($locale == 'zh_CN') {
                        printf("<span>技術・產品介紹</span>");
                    }
                    ?>
                </h1>
            </section><!-- ./p-recruit--banner -->
            <div class="p-service">
                <div class="p-service--ttl">
                    <h2 class="section-title-ep"><?php echo $queries_obj->name?></h2>
                </div>
                <div class="p-service--intro">
                    <?php the_field('pd_cat_desc', 'category_product_'.$queries_obj->term_id) ?>
                </div><!-- ./p-service--intro -->
                <div class="p-company--infor">
                    <ul class="anchor--list">
                        <?php 
                        if($locale == 'ja') {
                            printf('<li><a href="#subject01" class="link-anchor">製品ラインナップ</a></li>');
                            printf('<li><a href="#subject02" class="link-anchor">技術情報</a></li>');
                        } elseif ($locale == 'en_US') {
                            printf('<li><a href="#subject01" class="link-anchor">Product lineup</a></li>');
                            printf('<li><a href="#subject02" class="link-anchor">Technology Info.</a></li>');
                        } elseif ($locale == 'zh_CN') {
                            printf('<li><a href="#subject01" class="link-anchor">產品列表</a></li>');
                            printf('<li><a href="#subject02" class="link-anchor">技術情報</a></li>');
                        }
                        ?>
                    </ul><!-- .p-company--infor-bar -->
                    <div class="p-company--infor-cnt">
                        <div class="p-faq--row" id="subject01">
                            <?php 
                            if($locale == 'ja') {
                                printf('<h3 class="title-lv2">製品ラインナップ</h3>');
                            } elseif ($locale == 'en_US') {
                                printf('<h3 class="title-lv2">Product lineup</h3>');
                            } elseif ($locale == 'zh_CN') {
                                printf('<h3 class="title-lv2">產品列表</h3>');
                            }
                            ?>
                            <div class="p-service--products">
                                <ul class="p-service--products-list">
                                    <?php
                                    if (have_posts()): while (have_posts()) : the_post();
                                        $lineup = get_field('lineup');
                                        ?>
                                        <li class="p-service--products-item">
                                            <h3 class="title-lv3"><?php the_title()?></h3>
                                            <div class="p-service--products-item-inner">
                                                <div class="p-service--products-item-thumb">
                                                    <?php if ( has_post_thumbnail() ) : ?>
                                                        <?php the_post_thumbnail('large', array('class' => 'cover')); ?>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="p-service--products-item-cnt">
                                                    <h4 class="title-lv4"><?php echo $lineup['title']?></h4>
                                                    <p class="desc2"><?php echo nl2br($lineup['desc'])?></p>
                                                    <a href="<?php the_permalink();?>" class="link-border">
                                                        <?php
                                                        if ($locale == 'ja') {
                                                            print '<span>詳しくみる</span>';
                                                        } elseif ($locale == 'en_US') {
                                                            print '<span>Detail</span>';
                                                        } elseif ($locale == 'zh_CN') {
                                                            print '<span>詳細資訊</span>';
                                                        }
                                                        ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div><!-- .p-faq--row -->   
                        <div class="p-faq--row" id="subject02">
                            <div class="p-service--infor">
                                <?php the_field('pd_cat_info', 'category_product_'.$queries_obj->term_id) ?>
                            </div><!-- ./p-service--infor -->
                        </div><!-- .p-faq--row -->    
                    </div><!-- .p-company--infor-cnt -->
                </div><!-- .p-company--infor -->
            </div><!-- ./p-service -->
        </div>
    </div>
    
    <div class="align-center mgt-60">
        <?php 
        if($locale == 'ja') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url('product'), '技術・製品紹介に戻る');
        } elseif ($locale == 'en_US') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url('product'), 'Technology / product introduction');
        } elseif ($locale == 'zh_CN') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url('product'), '技術・產品介紹');
        }
        ?>
    </div>
</main>

<?php get_footer(); ?>

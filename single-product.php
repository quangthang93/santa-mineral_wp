<?php get_header(); ?>

<main class="main p-end">

    <div class="p-end--cnt">
        <div class="container">
            <div class="breadcrumbWrap">
                <div class="breadcrumb">
                    <?php
                    if($locale == 'ja') {
                        $txt_home = 'トップページ';
                    } elseif ($locale == 'en_US') {
                        $txt_home = 'Top Page';
                    } elseif ($locale == 'zh_CN') {
                        $txt_home = 'Top Page';
                    }
                    ?>
                    <ul>
                        <li><a href="<?php echo home_url()?>"><?php echo $txt_home?></a></li>
                        <?php 
                        if($locale == 'ja') {
                            printf('<li><a href="%s">%s</a></li>', home_url('service-product'), '技術・製品情報');
                        } elseif ($locale == 'en_US') {
                            printf('<li><a href="%s">%s</a></li>', home_url('service-product'), 'Technology / product introduction');
                        } elseif ($locale == 'zh_CN') {
                            printf('<li><a href="%s">%s</a></li>', home_url('service-product'), '技術・產品介紹');
                        }
                        ?>
                        <li><?php the_title();?></li>
                    </ul>
                </div>
            </div><!-- ./breadcrumbWrap -->
            <section class="p-end--banner type2">
                <h1 class="p-end--ttl">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/end/ttl-service.png" alt="">
                    <?php 
                    if($locale == 'ja') {
                        printf('<span>技術・製品情報</span>');
                    } elseif ($locale == 'en_US') {
                        printf('<span>Technology / product introduction</span>');
                    } elseif ($locale == 'zh_CN') {
                        printf('<span>技術・產品介紹</span>');
                    }
                    ?>
                </h1>
            </section><!-- ./p-recruit--banner -->
            <?php
            if (have_posts()): while (have_posts()) : the_post();
                $pd_cats = get_the_terms(get_the_ID(), 'category_product');
                $main_cat = get_post_meta( get_the_ID(), 'rank_math_primary_category_product', true );
                $grp_lineup = get_field('lineup');
                $grp_link = get_field('pd_link');
                $main_cat2 = reset($pd_cats);
                $main_cat_ID = in_array($main_cat, $main_cat2) ? $main_cat : $main_cat2->term_id;
                ?>
                <div class="p-service">
                    <div class="p-service--ttl">
                        <?php 
                        if ( $pd_cats && ! is_wp_error( $pd_cats ) ) :
                            foreach ($pd_cats as $pd_cat):
                                if($pd_cat->term_id == $main_cat) {
                                    printf('<h2 class="section-title-ep">%s</h2>', $pd_cat->name);
                                }else {
                                    printf('<h2 class="section-title-ep">%s</h2>', $pd_cat->name);
                                }
                                break;
                            endforeach;
                        endif;?>
                    </div>
                    <div class="p-service--cnt">
                        <div class="p-service--detail">

                            <div class="p-service--detail-product col2">
                                <div class="p-service--detail-product-infor col2-item">
                                    <h3 class="title-lv2"><?php the_title()?></h3>
                                    <p class="desc"><?php echo nl2br($grp_lineup['title'])?></p>
                                    <?php if($grp_link['title']) {?>
                                        <a href="<?php echo $grp_link['url']?>" class="link-pink"><span><?php echo $grp_link['title']?></span></a>
                                    <?php }?>
                                </div>
                                <div class="p-service--detail-product-thumb col2-item">
                                    <?php if ( has_post_thumbnail() ) : ?>
                                        <?php the_post_thumbnail('large'); ?>
                                    <?php endif; ?>
                                </div>
                            </div><!-- ./p-service--detail-product -->

                            <div class="p-service--detail-infor">
                                <?php
                                    if (have_rows('features')): while (have_rows('features')) : the_row();?>
                                    <div class="p-service--detail-row">
                                        <?php if(get_sub_field('heading')){?>
                                            <h4 class="title-lv3"><?php the_sub_field('heading')?></h4>
                                        <?php }?>
                                        <?php
                                        if (have_rows('items')) { $j=0;
                                            echo '<ul class="p-service--features">';
                                            while (have_rows('items')) : $j++; the_row();?>
                                                <li class="p-service--features-item">
                                                    <div class="p-service--features-ttlWrap">
                                                        <h5 class="p-service--features-item-ttl"><?php the_sub_field('title')?></h5>
                                                        <div class="p-service--features-item-thumb">
                                                            <img src="<?php the_sub_field('icon')?>">
                                                        </div>
                                                    </div>
                                                    <div class="p-service--features-cnt">
                                                        <p class="p-service--features-cnt-ttl"><?php the_sub_field('content')?></p>
                                                    </div>
                                                </li>
                                                <?php
                                            endwhile;
                                            echo '</ul>';
                                        }
                                        ?>
                                    </div><!-- ./p-service--detail-row -->
                                    <?php endwhile; ?>
                                <?php endif; ?>

                                <?php
                                    if (have_rows('points')): while (have_rows('points')) : the_row();?>
                                    <div class="p-service--detail-row">
                                        <?php if(get_sub_field('heading')){?>
                                            <h4 class="title-lv3"><?php the_sub_field('heading')?></h4>
                                        <?php }?>
                                        <?php if(get_sub_field('テキスト')){?>
                                            <p><?php the_sub_field('テキスト')?></p>
                                        <?php }?>
                                        <?php
                                        if (have_rows('items')) { $j=0;
                                            echo '<ul class="p-service--points">';
                                            while (have_rows('items')) : $j++; the_row();?>
                                                <li class="p-service--points-item">
                                                    <div class="p-service--points-numberWrap">
                                                        <span class="p-service--points-label stroke-white">POINT</span>
                                                        <span class="p-service--points-number stroke-pink"><?php echo $j;?></span>
                                                    </div>
                                                    <div class="p-service--points-cnt">
                                                        <div class="p-service--points-cnt-inner">
                                                            <p class="p-service--points-cnt-ttl"><?php the_sub_field('content')?></p>
                                                        </div>
                                                    </div>
                                                </li>
                                                <?php
                                            endwhile;
                                            echo '</ul>';
                                        }
                                        ?>
                                    </div><!-- ./p-service--detail-row -->
                                    <?php endwhile; ?>
                                <?php endif; ?>

                                <?php if (have_rows('description')): while (have_rows('description')) : the_row();?>
                                    <div class="p-service--detail-row">
                                        <?php if(get_sub_field('heading')){?>
                                            <h4 class="title-lv3"><?php the_sub_field('heading')?></h4>
                                        <?php }?>
                                        <div class="col2-75">
                                            <div class="col2-75--left">
                                                <p class="desc2"><?php echo nl2br(get_sub_field('desc'))?></p>
                                            </div>
                                        </div>
                                        <div class="align-center">
                                            <?php if(get_sub_field('image')) {?>
                                             	<img src="<?php the_sub_field('image')?>" alt="">
                                            <?php }?>
                                        </div>
                                    </div><!-- ./p-service--detail-row -->
                                    <?php endwhile; ?>
                                <?php endif; ?>

                                <?php if (have_rows('reports')): while (have_rows('reports')) : the_row();?>
                                    <div class="p-service--detail-row">
                                        <?php if(get_sub_field('heading')){?>
                                            <h4 class="title-lv3"><?php the_sub_field('heading')?></h4>
                                        <?php }?>
                                        <p class="desc2 mgb-30"><?php echo nl2br(get_sub_field('desc'))?></p>
                                        <?php
                                        if (have_rows('items')) {
                                            echo '<div class="col2">';
                                            while (have_rows('items')) : the_row();
                                                $report_img = get_sub_field('image');
                                                ?>
                                                <div class="col2-item">
                                                    <h3 class="title-lv4 mgb-15"><?php the_sub_field('heading')?></h3>
                                                    <div class="col2-item--thumb">
                                                        <?php
                                                        if(in_array('yes', get_sub_field('画像サイズ'))) {?>
                                                            <a href="<?php echo $report_img['url']?>" target="_blank">
                                                                <img src="<?php echo $report_img['sizes']['medium_large'] ?>" alt="">
                                                            </a>
                                                            <?php
                                                        } else {
                                                            printf("<img src='%s' />", $report_img['sizes']['medium_large']);
                                                        }
                                                        ?>
                                                    </div>
                                                    <p class="desc2 mgt-10"><?php echo (get_sub_field('テキスト')); ?></p>
                                                </div>
                                                <?php
                                            endwhile;
                                            echo '</div>';
                                        }
                                        ?>
                                    </div><!-- ./p-service--detail-row -->
                                    <?php endwhile; ?>
                                <?php endif; ?>

                                <?php if (have_rows('results')): while (have_rows('results')) : the_row();?>
                                    <div class="p-service--detail-row">
                                        <?php if(get_sub_field('heading')){?>
                                            <h4 class="title-lv3"><?php the_sub_field('heading')?></h4>
                                        <?php }?>
                                        <p class="desc2 mgb-20"><?php echo nl2br(get_sub_field('desc'))?></p>
                                        <?php if (have_rows('items')) {?>
                                            <div class="table custom-width01">
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th><?php echo do_shortcode('[ja]試験内容(報告日)[/ja][en]Content of test[/en][zh]實驗內容[/zh]')?></th>
                                                            <th><?php echo do_shortcode('[ja]評価[/ja][en]Evaluation[/en][zh]評價[/zh]')?></th>
                                                            <th><?php echo do_shortcode('[ja]試験結果[/ja][en]Test result[/en][zh]試驗結果[/zh]')?></th>
                                                            <th><?php echo do_shortcode('[ja]検査機関[/ja][en]Testing institution[/en][zh]試驗機關[/zh]')?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        while (have_rows('items')) : the_row();
                                                            $noi_dung = get_sub_field('noi_dung');
                                                            ?>
                                                            <tr>
                                                                <td data-label="<?php echo do_shortcode('[ja]試験内容(報告日)[/ja][en]Content of test[/en][zh]實驗內容[/zh]')?>">
                                                                    <a href="<?php echo $noi_dung['file']?>" target="_blank" class="link-icon pdf"><?php echo $noi_dung['file_name']?></a>
                                                                    <p class="desc2"><?php echo $noi_dung['thoi_gian']?></p>
                                                                </td>
                                                                <td data-label="<?php echo do_shortcode('[ja]評価[/ja][en]Evaluation[/en][zh]評價[/zh]')?>"><?php the_sub_field('danh_gia')?></td>
                                                                <td data-label="<?php echo do_shortcode('[ja]試験結果[/ja][en]Test result[/en][zh]試驗結果[/zh]')?>"><?php echo nl2br(get_sub_field('ket_qua'))?></td>
                                                                <td data-label="<?php echo do_shortcode('[ja]検査機関[/ja][en]Testing institution[/en][zh]試驗機關[/zh]')?>"><?php the_sub_field('kensa_kikan')?></td>
                                                            </tr>
                                                            <?php
                                                        endwhile;
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php }?>
                                    </div><!-- ./p-service--detail-row -->
                                    <?php endwhile; ?>
                                <?php endif; ?>

                            </div><!-- ./p-service--detail-infor   -->
                            
                        </div><!-- ./p-service--detail -->
                        <div class="p-service--infor">
                            <?php the_field('pd_cat_info', 'category_product_'.$main_cat_ID) ?>
                        </div><!-- ./p-service--infor -->
                    </div><!-- ./p-service--cnt -->
                </div><!-- ./p-service -->
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
    
    <div class="align-center mgt-60">
        <?php 
        if($locale == 'ja') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url('product'), '技術・製品紹介に戻る');
        } elseif ($locale == 'en_US') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url('product'), 'Technology / product introduction');
        } elseif ($locale == 'zh_CN') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url('product'), '技術・產品介紹');
        }
        ?>
    </div>
</main>

<?php get_footer(); ?>

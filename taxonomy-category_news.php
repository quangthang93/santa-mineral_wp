<?php get_header(); ?>

<main class="main p-end">

    <div class="p-end--cnt">
        <div class="container">
            <div class="breadcrumbWrap">
                <div class="breadcrumb">
                    <?php wp_breadcrumb() ?>
                </div>
            </div><!-- ./breadcrumbWrap -->
            <section class="p-end--banner type2">
                <h1 class="p-end--ttl">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/end/ttl-news.png" alt="">
                    <?php 
                    if($locale == 'ja') {
                        printf('<span>ニュース・レポート</span>');
                    } elseif ($locale == 'en_US') {
                        printf('<span>News & Report</span>');
                    } elseif ($locale == 'zh_CN') {
                        printf('<span>News & Report</span>');
                    }
                    ?>
                </h1>
            </section><!-- ./p-recruit--banner -->
            <?php $queries_obj = get_queried_object();?>
            <div class="p-news">
                <div class="p-news--cnt">
                    <ul class="p-top--news-list">
                        <?php
                        $y = isset($_GET['y'])? absint($_GET['y']) : '';
                        $cond_time = array(
                            array(
                                'year' => $y,
                            ),
                        );
                        $args['post_type'] = 'news';
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $args['paged'] = $paged;
                        $args['post_status'] = 'publish';
                        $args['posts_per_page'] = 12;
                        $args['orderby'] = 'date';
                        $args['order'] = 'DESC';
                        $args['date_query'] = $cond_time;

                        $news_query = null;
                        $news_query = new WP_Query($args);
                        if (have_posts()): while (have_posts()) : the_post();
                            $news_cat = get_the_terms(get_the_ID(), 'category_news');                            
                            $main_cat = get_post_meta( get_the_ID(), 'rank_math_primary_category_news', true );
                            ?>
                        
                            <li class="p-top--news-item">
                                <a href="<?php the_permalink()?>" class="link">
                                    <div class="p-top--news-item-thumbWrap">
                                        <?php 
                                        if ( $news_cat && ! is_wp_error( $news_cat ) ) :
                                            foreach ($news_cat as $news_cat_data):
                                                if($news_cat_data->term_id == $main_cat) {
                                                    printf('<span class="label type2">%s</span>', $news_cat_data->name);
                                                }else {
                                                    printf('<span class="label type2">%s</span>', $news_cat_data->name);
                                                }
                                                break;
                                            endforeach;
                                        endif;?>                                        
                                        <div class="p-top--news-item-thumb">
                                            <?php if ( has_post_thumbnail() ) : ?>
                                                <?php the_post_thumbnail('medium_large', array('class' => 'cover')); ?>
                                            <?php else:?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/img/no-img.png" class="cover" />
                                            <?php endif; ?>
                                        </div>
                                        <div class="p-top--news-item-cnt">
                                            <p class="date"><?php the_time('Y.m.d');?></p>
                                            <p class="p-top--news-item-ttl"><?php the_title()?></p>
                                        </div>
                                    </div>
                                </a>
                            </li>                                                                   
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </ul>
                    <?php pagination($news_query, 4, 12)?>
                </div><!-- /.p-news--cnt -->
                <div class="p-news--sidebar">
                    <?php 
                    if($locale == 'ja') {
                        printf('<p class="title-bold mgb-20">カテゴリー</p>');
                    } elseif ($locale == 'en_US') {
                        printf('<p class="title-bold mgb-20">Category</p>');
                    } elseif ($locale == 'zh_CN') {
                        printf('<p class="title-bold mgb-20">Category</p>');
                    }
                    ?>
                    <ul class="p-news--sidebar-list">
                        <?php
                        $news_terms = get_terms('category_news');
                        foreach ($news_terms as $news_term) {
                            $term_link = get_term_link($news_term, 'category_news');?>
                        
                            <li class="link"><a href="<?php echo $term_link;?>"><?php echo $news_term->name;?></a></li>
                        <?php } ?>                        
                    </ul>
                    <?php 
                    if($locale == 'ja') {
                        printf('<p class="title-bold mgb-20">過去のお知らせ</p>');
                    } elseif ($locale == 'en_US') {
                        printf('<p class="title-bold mgb-20">Archive</p>');
                    } elseif ($locale == 'zh_CN') {
                        printf('<p class="title-bold mgb-20">Archive</p>');
                    }
                    ?>
                    <select name="year" class="select" onchange="document.location.href='?y='+this.options[this.selectedIndex].value;">
                        <?php 
                        if($locale == 'ja') {
                            printf('<option value="">掲載年で絞り込む</option>');
                        } elseif ($locale == 'en_US') {
                            printf('<option value="">Please select</option>');
                        } elseif ($locale == 'zh_CN') {
                            printf('<option value="">Please select</option>');
                        }
                        ?>
                        <?php
                        $start_year = 2019;
                        for ($curr_Y = date('Y'); $curr_Y >= $start_year; $curr_Y--) :
                            ?>
                            <option value="<?php echo $curr_Y ?>" <?php selected( $y, $curr_Y )?>><?php echo $curr_Y ?></option>
                        <?php endfor; ?>                        
                    </select>
                </div><!-- .p-news--sidebar -->
            </div><!-- ./p-news -->
        </div>
    </div>
    
    <div class="align-center mgt-60">
        <?php 
        if($locale == 'ja') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url(), 'トップページへ戻る');
        } elseif ($locale == 'en_US') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url(), 'Back to Top');
        } elseif ($locale == 'zh_CN') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url(), 'Back to Top');
        }
        ?>
    </div>
</main>

<?php get_footer(); ?>

<?php get_header(); ?>

<main class="main p-end">
    <div class="p-end--cnt">
        <div class="container">
            <div class="breadcrumbWrap">
                <div class="breadcrumb">
                    <?php wp_breadcrumb() ?>
                </div>
            </div><!-- ./breadcrumbWrap -->
            <section class="p-end--banner type2">
                <h1 class="p-end--ttl">
                    <img src="<?php the_field('title_en'); ?>" alt="">
                    <span><?php the_title() ?></span>
                </h1>
            </section><!-- ./p-recruit--banner -->
            <div class="p-tech">
                <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                    <section class="p-tech--intro">
                        <div class="p-tech--intro-bg">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/bg-qq2.svg" alt="">
                        </div>
                        <div class="p-tech--intro-inner">
                            <div class="title-boxWrap heading">
                                <div class="ani-border type2">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                                <h3 class="title-box type2"><?php echo nl2br(get_field('tech_title'))?></h3>
                            </div>
                            <div class="p-tech--intro-cnt">
                                <?php the_content(); ?>
                            </div>
                        </div><!-- ./p-tech--intro-inner -->
                        <div class="p-tech--intro-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-water.png" alt="">
                        </div>
                    </section><!-- ./p-tech--intro -->
                    <section class="p-tech--function">
                        <?php
                        if (have_rows('tech_box_performance')):
                            while (have_rows('tech_box_performance')) : the_row();
                                ?>

                                <div class="title-boxWrap heading">
                                    <div class="ani-border type2">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                    <?php 
                                    if($locale == 'ja') {
                                        printf('<h3 class="title-box type2">基本性能・機能</h3>');
                                    } elseif ($locale == 'en_US') {
                                        printf('<h3 class="title-box type2">Basic performance / function</h3>');
                                    } elseif ($locale == 'zh_CN') {
                                        printf('<h3 class="title-box type2">基本性能・機能</h3>');
                                    }
                                    ?>
                                </div>
                                <p class="title-bold"><?php the_sub_field('desc') ?></p>
                                <div class="p-tech--function-inner">
                                    <?php
                                    if (have_rows('items')): $j = 0;
                                        while (have_rows('items')) : $j++;
                                            the_row();
                                            if ($j == 1) {
                                                ?>
                                                <div class="p-tech--function-row<?php printf('%02d', $j) ?>">
                                                    <div class="p-tech--function-row<?php printf('%02d', $j) ?>-img">
                                                        <img src="<?php the_sub_field('img') ?>" alt="">
                                                    </div>
                                                    <div class="title-boxWrap">
                                                        <div class="ani-border2 type2">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>

                                                        <div class="p-tech--function-row<?php printf('%02d', $j) ?>-cnt">
                                                            <h3 class="title-quad type2 function"><?php the_sub_field('title') ?></h3>
                                                            <p class="desc2"><?php echo nl2br(get_sub_field('content')) ?></p>
                                                        </div>
                                                    </div>
                                                </div><!-- ./p-tech--function-row01 -->

                                            <?php } elseif ($j == 2) { ?>
                                                <div class="p-tech--function-row02">
                                                    <div class="p-tech--function-row02-img">
                                                        <img src="<?php the_sub_field('img') ?>" alt="">
                                                    </div>
                                                    <div class="title-boxWrap">
                                                        <div class="ani-border3 type2">
                                                            <span></span>
                                                        </div>

                                                        <div class="p-tech--function-row02-cnt">
                                                            <h3 class="title-quad type2 function"><?php the_sub_field('title') ?></h3>
                                                            <p class="desc2"><?php echo nl2br(get_sub_field('content')) ?></p>
                                                        </div>
                                                    </div>
                                                </div><!-- ./p-tech--function-row02 -->
                                            <?php } elseif ($j == 3) { ?>
                                                <div class="p-tech--function-row03">
                                                    <div class="p-tech--function-row03-img">
                                                        <img src="<?php the_sub_field('img') ?>" alt="">
                                                    </div>
                                                    <div class="title-boxWrap">
                                                        <div class="ani-border4 type2">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>

                                                        <div class="p-tech--function-row03-cnt">
                                                            <h3 class="title-quad type2 function"><?php the_sub_field('title') ?></h3>
                                                            <p class="desc2"><?php echo nl2br(get_sub_field('content')) ?></p>
                                                        </div>
                                                    </div>
                                                </div><!-- ./p-tech--function-row03 -->
                                            <?php } elseif ($j == 4) { ?>
                                                <div class="p-tech--function-row04">
                                                    <div class="p-tech--function-row04-img">
                                                        <img src="<?php the_sub_field('img') ?>" alt="">
                                                    </div>
                                                    <div class="title-boxWrap">
                                                        <div class="ani-border5 type2">
                                                            <span></span>
                                                        </div>

                                                        <div class="p-tech--function-row04-cnt">
                                                            <h3 class="title-quad type2 function"><?php the_sub_field('title') ?></h3>
                                                            <p class="desc2"><?php echo nl2br(get_sub_field('content')) ?></p>
                                                        </div>
                                                    </div>
                                                </div><!-- ./p-tech--function-row04 -->
                                                <?php
                                            }
                                        endwhile;
                                    endif;
                                    ?>
                                </div>
                                <?php
                            endwhile;
                        endif;
                        ?>
                    </section><!-- ./p-tech--function -->
                    <section class="p-tech--activity">
                        <div class="title-boxWrap heading">
                            <div class="ani-border type2">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <?php 
                            if($locale == 'ja') {
                                printf('<h3 class="title-box type2">活用領域</h3>');
                            } elseif ($locale == 'en_US') {
                                printf('<h3 class="title-box type2">Available Area</h3>');
                            } elseif ($locale == 'zh_CN') {
                                printf('<h3 class="title-box type2">活用領域</h3>');
                            }
                            ?>
                        </div>
                        <p class="title-bold">
                            <?php 
                            if($locale == 'ja') {
                                print 'QQテクノロジーは、生活環境や医療領域、農工業、環境浄化など多岐にわたる分野での実用化を想定して開発されています。';
                            } elseif ($locale == 'en_US') {
                                print 'QQ Technology has been developed for practical use in a wide range of fields such as living environment, medical field, agriculture and industry, and environmental purification.';
                            } elseif ($locale == 'zh_CN') {
                                print 'QQ Technology已被開發用於生活環境、醫療領域、農業和工業以及環境淨化等廣泛領域的實際應用。';
                            }
                            ?>
                        </p>
                        <div class="p-tech--activity-inner">
                            <?php
                            if (have_rows('tech_layout')):
                                while (have_rows('tech_layout')) : the_row();
                                    switch (get_row_layout()):
                                        case 'l_environment':
                                            ?>
                                            <div class="p-tech--activity-row">
                                                <div class="p-tech--activity-left">
                                                    <div class="_img">
                                        
                                                        <?php
                                                        if ($locale == 'en_US') {
                                                            printf('<h4 class="_ttl _ttl_en1">');
                                                        } else {
                                                            printf('<h4 class="_ttl">');
														}
                                                        ?>
                                        
                                                            <?php
                                                            if ($locale == 'ja') {
                                                                printf('<img src="%s" alt="%s" />', get_template_directory_uri(). '/assets/images/end/technology/ttl01.png', '生活環境');
                                                            } elseif ($locale == 'en_US') {
                                                                printf('<img src="%s" alt="%s" />', get_template_directory_uri(). '/assets/images/end/technology/en/ttl01.png', 'Life / Medical Care');
                                                            } elseif ($locale == 'zh_CN') {
                                                                printf('<img src="%s" alt="%s" />', get_template_directory_uri(). '/assets/images/end/technology/china/ttl01.png', 'Environmental Purification');
                                                            }
                                                            ?>
                                                        </h4>
                                                        <img src="<?php the_sub_field('image') ?>">
                                                    </div>
                                                </div>
                                                <div class="p-tech--activity-right">
                                                    <div class="p-tech--activity-right-list">
                                                        <?php
                                                        if (have_rows('items')):
                                                            while (have_rows('items')) : the_row();
                                                                ?>
                                                                <div class="p-tech--activity-right-item">
                                                                    <div class="p-tech--activity-right-inner">
                                                                        <div class="p-tech--activity-right-head">
                                                                            <h5 class="title-lv4 js-equalTtlLv4"><?php the_sub_field('title')?></h5>
                                                                        </div>
                                                                        <div class="p-tech--activity-right-cnt">
                                                                            <p class="desc2 js-equalDesc"><?php echo nl2br(get_sub_field('content'))?></p>
                                                                            <?php if(get_sub_field('url')) {?>
                                                                                <a href="<?php the_sub_field('url')?>" class="link-border">
                                                                                    <span>
                                                                                        <?php echo do_shortcode('[ja]詳しくみる[/ja][en]Detail[/en][zh]詳細資訊[/zh]')?>
                                                                                    </span>
                                                                                </a>
                                                                            <?php }?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- ./p-tech--activity-right-item -->
                                                                <?php
                                                            endwhile;
                                                        endif;
                                                        ?>
                                                    </div><!-- ./p-tech--activity-right-list -->
                                                </div><!-- ./p-tech--activity-right -->
                                            </div><!-- ./p-tech--activity-row -->
                                            <?php
                                            break;
                                            case 'l_beauty_health':?>
                                                <div class="p-tech--activity-row">
                                                    <div class="p-tech--activity-left type2">
                                                        <div class="_img">
                                                
                                                        <?php
                                                        if ($locale == 'en_US') {
                                                            printf('<h4 class="_ttl _ttl_en2">');
                                                        } else {
                                                            printf('<h4 class="_ttl">');
														}
                                                        ?>
                                                                <?php
                                                                if ($locale == 'ja') {
                                                                    printf('<img src="%s" alt="%s" />', get_template_directory_uri(). '/assets/images/end/technology/ttl02.png', '美容・健康');
                                                                } elseif ($locale == 'en_US') {
                                                                    printf('<img src="%s" alt="%s" />', get_template_directory_uri(). '/assets/images/end/technology/en/ttl02.png', 'Beauty / Health');
                                                                } elseif ($locale == 'zh_CN') {
                                                                    printf('<img src="%s" alt="%s" />', get_template_directory_uri(). '/assets/images/end/technology/ttl02.png', '美容・健康');
                                                                }
                                                                ?>
                                                            </h4>
                                                            <img src="<?php the_sub_field('image') ?>">
                                                        </div>
                                                    </div>
                                                    <div class="p-tech--activity-right">
                                                        <div class="p-tech--activity-right-list">
                                                            <?php
                                                            if (have_rows('items')):
                                                                while (have_rows('items')) : the_row();
                                                                    ?>
                                                                    <div class="p-tech--activity-right-item type2">
                                                                        <div class="p-tech--activity-right-inner">
                                                                            <div class="p-tech--activity-right-head">
                                                                                <h5 class="title-lv4 js-equalTtlLv4"><?php the_sub_field('title') ?></h5>
                                                                            </div>
                                                                            <div class="p-tech--activity-right-cnt">
                                                                                <p class="desc2 js-equalDesc"><?php echo nl2br(get_sub_field('content')) ?></p>
                                                                                <?php if (get_sub_field('url')) { ?>
                                                                                    <a href="<?php the_sub_field('url') ?>" class="link-border">
                                                                                        <span>
                                                                                        <?php echo do_shortcode('[ja]詳しくみる[/ja][en]Detail[/en][zh]詳細資訊[/zh]')?>
                                                                                        </span>
                                                                                    </a>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- ./p-tech--activity-right-item -->
                                                                    <?php
                                                                endwhile;
                                                            endif;
                                                            ?>
                                                        </div><!-- ./p-tech--activity-right-list -->
                                                    </div><!-- ./p-tech--activity-right -->
                                                </div><!-- ./p-tech--activity-row -->
                                            <?php
                                            break;
                                            case 'l_agricultural_jobs':?>
                                                <div class="p-tech--activity-row">
                                                    <div class="p-tech--activity-left type3">
                                                        <div class="_img">
                                                
                                                        <?php
                                                        if ($locale == 'en_US') {
                                                            printf('<h4 class="_ttl _ttl_en3">');
                                                        } else {
                                                            printf('<h4 class="_ttl">');
														}
                                                        ?>
                                                        
                                                                <?php
                                                                if ($locale == 'ja') {
                                                                    printf('<img src="%s" alt="%s" />', get_template_directory_uri(). '/assets/images/end/technology/ttl03.png', '農業・畜産');
                                                                } elseif ($locale == 'en_US') {
                                                                    printf('<img src="%s" alt="%s" />', get_template_directory_uri(). '/assets/images/end/technology/en/ttl03.png', 'Agriculture / Livestock');
                                                                } elseif ($locale == 'zh_CN') {
                                                                    printf('<img src="%s" alt="%s" />', get_template_directory_uri(). '/assets/images/end/technology/ttl03.png', '農業・畜産');
                                                                }
                                                                ?>
                                                            </h4>
                                                            <img src="<?php the_sub_field('image') ?>">
                                                        </div>
                                                    </div>
                                                    <div class="p-tech--activity-right">
                                                        <div class="p-tech--activity-right-list">
                                                            <?php
                                                            if (have_rows('items')):
                                                                while (have_rows('items')) : the_row();
                                                                    ?>
                                                                    <div class="p-tech--activity-right-item type3">
                                                                        <div class="p-tech--activity-right-inner">
                                                                            <div class="p-tech--activity-right-head">
                                                                                <h5 class="title-lv4 js-equalTtlLv4"><?php the_sub_field('title')?></h5>
                                                                            </div>
                                                                            <div class="p-tech--activity-right-cnt">
                                                                                <p class="desc2 js-equalDesc"><?php echo nl2br(get_sub_field('content'))?></p>
                                                                                <?php if(get_sub_field('url')) {?>
                                                                                    <a href="<?php the_sub_field('url')?>" class="link-border">
                                                                                        <span>
                                                                                            <?php echo do_shortcode('[ja]詳しくみる[/ja][en]Detail[/en][zh]詳細資訊[/zh]')?>
                                                                                        </span>
                                                                                    </a>
                                                                                <?php }?>
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- ./p-tech--activity-right-item -->
                                                                    <?php
                                                                endwhile;
                                                            endif;
                                                            ?>
                                                        </div><!-- ./p-tech--activity-right-list -->
                                                    </div><!-- ./p-tech--activity-right -->
                                                </div><!-- ./p-tech--activity-row -->
                                            <?php
                                            break;
                                            case 'l_purification':?>
                                                <div class="p-tech--activity-row">
                                                    <div class="p-tech--activity-left type4">
                                                        <div class="_img">
                                                
                                                        <?php
                                                        if ($locale == 'en_US') {
                                                            printf('<h4 class="_ttl _ttl_en4">');
                                                        } else {
                                                            printf('<h4 class="_ttl">');
														}
                                                        ?>
                                                                <?php
                                                                if ($locale == 'ja') {
                                                                    printf('<img src="%s" alt="%s" />', get_template_directory_uri(). '/assets/images/end/technology/ttl04.png', '環境浄化');
                                                                } elseif ($locale == 'en_US') {
                                                                    printf('<img src="%s" alt="%s" />', get_template_directory_uri(). '/assets/images/end/technology/en/ttl04.png', '環境淨化');
                                                                } elseif ($locale == 'zh_CN') {
                                                                    printf('<img src="%s" alt="%s" />', get_template_directory_uri(). '/assets/images/end/technology/china/ttl04.png', '環境淨化');
                                                                }
                                                                ?>
                                                            </h4>
                                                            <img src="<?php the_sub_field('image') ?>">
                                                        </div>
                                                    </div>
                                                    <div class="p-tech--activity-right">
                                                        <div class="p-tech--activity-right-list">
                                                            <?php
                                                            if (have_rows('items')):
                                                                while (have_rows('items')) : the_row();
                                                                    ?>
                                                                    <div class="p-tech--activity-right-item type4">
                                                                        <div class="p-tech--activity-right-inner">
                                                                            <div class="p-tech--activity-right-head">
                                                                                <h5 class="title-lv4 js-equalTtlLv4"><?php the_sub_field('title')?></h5>
                                                                            </div>
                                                                            <div class="p-tech--activity-right-cnt">
                                                                                <p class="desc2 js-equalDesc"><?php echo nl2br(get_sub_field('content'))?></p>
                                                                                <?php if(get_sub_field('url')) {?>
                                                                                    <a href="<?php the_sub_field('url')?>" class="link-border">
                                                                                        <span>
                                                                                            <?php echo do_shortcode('[ja]詳しくみる[/ja][en]Detail[/en][zh]詳細資訊[/zh]')?>
                                                                                        </span>
                                                                                    </a>
                                                                                <?php }?>
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- ./p-tech--activity-right-item -->
                                                                    <?php
                                                                endwhile;
                                                            endif;
                                                            ?>
                                                        </div><!-- ./p-tech--activity-right-list -->
                                                    </div><!-- ./p-tech--activity-right -->
                                                </div><!-- ./p-tech--activity-row -->
                                            <?php
                                            break;
                                    endswitch;
                                endwhile;
                            endif;
                            ?>
                        </div><!-- ./p-tech--activity-inner -->
                    </section><!-- ./p-tech--activity -->
                                        
                <?php endwhile; ?>                    
                <?php endif; ?>
            </div><!-- ./p-tech -->
        </div>
    </div>    
    
    <div class="align-center mgt-60">
        <?php printf("<a href='%s' class='viewmore2'>%s</a>", home_url(), do_shortcode('[ja]トップページへ戻る[/ja][en]Back to Top[/en][zh]Back to Top[/zh]')); ?>
    </div>
</main>

<?php get_footer(); ?>

<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <title><?php wp_title(''); ?></title>

        <link href="//www.google-analytics.com" rel="dns-prefetch">

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        <?php wp_head(); ?>

    </head>
    <body <?php body_class(); ?>>
        <div class="wrapper">

            <header class="header js-header">
                <h1 class="header-logo link">
                    <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/logo-header.png" alt="Santa Mineral （サンタミネラル）"></a>
                </h1>
                <div class="header-menu js-menu">
                    <div class="top-navWrap">
                        <?php main_nav(); ?>
                        <div class="bogo-language-switcherWrap">
                            <p class="bogo-language-switcher-label">LANGUAGE</p>
                            <?php echo do_shortcode( '[bogo]' ); ?>
                        </div>
                        <?php 
                        global $locale;
                        if($locale == 'ja') {
                            printf("<a href='%s' class='header-menu--contact'>%s</a>", home_url('contact'), 'お問合わせ');
                        } elseif ($locale == 'en_US') {
                            printf("<a href='%s' class='header-menu--contact'>%s</a>", home_url('contact'), 'Contact');
                        } elseif ($locale == 'zh_CN') {
                            printf("<a href='%s' class='header-menu--contact'>%s</a>", home_url('contact'), '諮詢');
                        }
                        ?>
                        <div class="header-access sp-only">
                            <div class="header-access--logo">
                                <a class="link" href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/logo-header.png" alt=""></a>
                            </div>
                            <p class="header-access--address">〒105-0013 東京都港区浜松町二丁目6番4号ガリシア浜松町1401号</p>
                            <a class="header-access--direct viewmore" href="https://goo.gl/maps/MUd5xeVLctQzVVhr7" target="_blank">Google Map</a>
                        </div>
                    </div>
                </div>
                <a class="header-ctrl js-menuTrigger" href="javascript:void(0);">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
                <div class="header-menu--overlay js-menuOverlay sp-only"></div>
            </header><!-- ./header -->

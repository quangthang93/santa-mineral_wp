<?php get_header(); ?>

<main class="main p-end">
    <div class="p-end--cnt">
        <div class="container">
            <div class="breadcrumbWrap">
                <div class="breadcrumb">
                    <?php wp_breadcrumb()?>
                </div>
            </div><!-- ./breadcrumbWrap -->

            <section class="p-end--banner type2">
                <h1 class="p-end--ttl">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/end/ttl-faq.png" alt="">
                    <?php 
                    if($locale == 'ja') {
                        printf("<span>よくあるご質問</span>");
                    } elseif ($locale == 'en_US') {
                        printf("<span>FAQ</span>");
                    } elseif ($locale == 'zh_CN') {
                        printf("<span>常見問題</span>");
                    }
                    ?>
                </h1>
            </section><!-- ./p-recruit--banner -->
            <?php
            $args = [
                'posts_per_page' => 12,
                'post_status' => 'publish',
                'post_type' => 'faq',                    
            ];
            ?>
            <div class="p-faq">
                <div class="p-company--infor">
                    <ul class="anchor--list">
                        <?php
                        $faq_query = null;
                        $faq_query = new WP_Query($args);
                        if ($faq_query->have_posts()):
                            while ($faq_query->have_posts()) : $faq_query->the_post();?>
                                <li>
                                    <a href="#target-<?php the_ID()?>" class="link-anchor"><?php the_title();?></a>
                                </li>
                            <?php
                            endwhile;
                            wp_reset_postdata();
                        endif;
                        ?>
                    </ul><!-- .p-company--infor-bar -->
                    <div class="p-company--infor-cnt">
                        <?php
                        if ($faq_query->have_posts()):
                            while ($faq_query->have_posts()) : $faq_query->the_post();?>

                                <div class="p-faq--row" id="target-<?php the_ID()?>">
                                    <h2 class="title-lv2"><?php the_title();?></h2>
                                    <?php if (have_rows('faq_items')): ?>
                                            <?php while (have_rows('faq_items')): the_row();
                                                ?>
                                                <div class="accordion">
                                                    <div class="accordion-label js-accorLabel"><?php the_sub_field('title') ?></div>
                                                    <div class="accordion-cnt js-accorCnt">
                                                        <p><?php echo nl2br(get_sub_field('content')) ?></p>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                    <?php endif; ?>

                                </div><!-- .p-faq--row -->
                            <?php
                            endwhile;
                            wp_reset_postdata();
                        endif;
                        ?>
                    </div><!-- .p-company--infor-cnt -->
                </div><!-- .p-company--infor -->
            </div><!-- ./p-faq -->
        </div>
    </div>
    <div class="align-center mgt-60">
        <?php 
        if($locale == 'ja') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url(), 'トップページへ戻る');
        } elseif ($locale == 'en_US') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url(), 'Back to Top');
        } elseif ($locale == 'zh_CN') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url(), 'Back to Top');
        }
        ?>
    </div>

</main><!-- ./main -->

<?php get_footer(); ?>

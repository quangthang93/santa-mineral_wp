        <footer class="footer js-footer">
            <a href="javascript:void(0)" class="backtop js-backtop">
                <div class="backtop-inner"><span>PAGE TOP</span></div>
            </a>
            <div class="footer-cnt">
                <div class="footer-head--infor">
                    <div class="footer-head--contact">
                        <p class="footer-head--contact-ttl">CONTACT</p>
                        <?php
                        global $locale;
                        if($locale == 'ja') {?>
                            <p class="desc">当社製品・技術に関するご質問やご相談など、お気軽にお問い合わせください。</p>
                            <a href="<?php echo home_url('/contact')?>" class="link-pink"><span>お問い合わせフォーム</span></a>
                        <?php
                        } elseif ($locale == 'en_US') {?>
                            <p class="desc">Please feel free to contact us with any questions or inquiries regarding our products and technology.</p>
                            <a href="<?php echo home_url('/contact')?>" class="link-pink"><span>Contact form</span></a>
                        <?php
                        } elseif ($locale == 'zh_CN') {?>
                            <p class="desc">如果您對我們的產品和技術有任何疑問，請隨時與我們聯繫。</p>
                            <a href="<?php echo home_url('/contact')?>" class="link-pink"><span>聯絡方式</span></a>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="footer-head--access">
                        <div class="footer-head--logo">
                            <a class="link" href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/logo-header.png" alt=""></a>
                        </div>
                        <div class="footer-head--addressWrap">
                            <?php
                            if($locale == 'ja') {
                                printf('<p class="footer-head--address">〒105-0013 東京都港区浜松町二丁目6番4号ガリシア浜松町1401号</p>');
                            } elseif ($locale == 'en_US') {
                                printf('<p class="footer-head--address">〒105-0013 2 - 6 - 4 -1401 Hamamatsu -cho, Minato -ku, Tokyo</p>');
                            } elseif ($locale == 'zh_CN') {
                                printf('<p class="footer-head--address">〒105-0013 東京都港區濱松町2-6-4-1401</p>');
                            }
                            ?>
                            
                        </div>
                    </div>
                </div>
                <?php footer_nav() ?>                
            </div>
            <div class="footer-copy">
                <p>Copyright © <?php echo date('Y'); ?> Santa Mineral All rights reserved.</p>
            </div>
        </footer><!-- ./footer -->
    </div>

    <?php wp_footer(); ?>
    </body>
</html>

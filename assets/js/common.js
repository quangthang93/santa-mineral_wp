jQuery(function($) {

  /* --------------------
  GLOBAL VARIABLE
  --------------------- */
  // Selector
  // var $pageTop = $('.js-pageTop');

  // Init Value
  var breakpointSP = 767,
      breakpointTB = 1050,
      wWindow = $(window).outerWidth();


  /* --------------------
  FUNCTION COMMON
  --------------------- */
  // Menu trigger
  var triggerMenu = function() {
    $('.js-menuTrigger').click(function() {
      $('body').toggleClass('fixed');
      $(this).toggleClass('open');
      $('.js-menu').toggleClass('open');
      $('.js-menuOverlay').toggleClass('open');
    });
  }

  // Setting anchor link
  var anchorLink = function() {
    // Scroll to section
    $('a[href^="#"]').not("a[class*='carousel-control-']").click(function() {
      var href= $(this).attr("href");
      var hash = href == "#" || href == "" ? 'html' : href;
      var position = $(hash).offset().top - 100;
      $('body,html').stop().animate({scrollTop:position}, 1000);
      return false;
    });
  }

  var scrollLoad = function() {
   var scroll = $(this).scrollTop();
    $('.fadeup, .fadeleft, .faderight, .fadein, .ani-border, .ani-border2, .ani-border3, .ani-border4, .ani-border5').each(function() {
      var elemPos = $(this).offset().top;
      var windowHeight = $(window).height();
      if (scroll > elemPos - windowHeight + 100) {
        $(this).addClass('in');
      }
    });
  }

  // Tabs Control
  var tabsControl = function() {
    var $tabsNav = $('.js-tabsNav'),
        $tabsItem = $('.js-tabsItem'),
        $tabsCnt = $('.js-tabsCnt'),
        $tabsPanel = $('.js-tabsPanel');

    // Setting first view
    $tabsPanel.hide();
    $tabsCnt.each(function () {
        $(this).find($tabsPanel).eq(0).show();
    });
    $tabsNav.each(function () {
        $(this).find($tabsItem).eq(0).addClass('active');
    });

    // Click event
    $tabsItem.on('click', function () {
      var tMenu = $(this).parents($tabsNav).find($tabsItem);
      var tCont = $(this).parents($tabsNav).next($tabsCnt).find($tabsPanel);
      var index = tMenu.index(this);
      tMenu.removeClass('active');
      $(this).addClass('active');
      tCont.hide();
      tCont.eq(index).show();
    });
  } 

  // Trigger Accordion
  var triggerAccordion = function() {
    var $accorLabel = $('.js-accorLabel'),
        $accorCnt = $('js-accorCnt');
    $accorLabel.click(function () {
      $(this).toggleClass('active').siblings($accorCnt).slideToggle();
    });   
  } 


  // Slider init
  // Slider (Restaurant detail page)
  var initSliders = function() {
    var $slide01 = $('.js-newsSliders'); 

    // Slider 01
    if ($slide01.length) {
      // Tabs slider
      $slide01.slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        swipeToSlide: true,
        // fade: true,
        prevArrow: '<a class="slick-prev" href="javascript:void(0)"><span></span></a>',
        nextArrow: '<a class="slick-next" href="javascript:void(0)"><span></span></a>',
        responsive: [
        {
          breakpoint: 1050,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            variableWidth: true,
            centerMode: true,
          }
        }
        ]
      });
    }
  }


  // Backtop click
  var clickBacktop = function() {
    $('.js-backtop').click(function(e) {
      $('html,body').animate({ scrollTop: 0 }, 300);
    });
  }

  // Trigger backtop
  var triggerBacktop = function() {
    if ($(this).scrollTop() > 200) {
      $('.js-backtop').fadeIn(200);
    } else {
      $('.js-backtop').fadeOut(200);
    }

    if ($(window).scrollTop() + $(window).height() < $(document).height() - $(".js-footer").height()) {
      $('.js-backtop').removeClass('fixed');
    } else {
      $('.js-backtop').addClass('fixed');
    }
  }

  // Trigger background for header
  var triggerBackground = function() {
    if ($(this).scrollTop() > 100) {
      $('.js-header').addClass('bg-white');
    } else {
      $('.js-header').removeClass('bg-white');
    }
  }

  var matchHeight = function() {
    var $elem01 = $('.js-equalDesc');
    var $elem02 = $('.js-equalTtlLv4');
    var $elem03 = $('.p-service--features-item-ttl');
    var $elem04 = $('.p-service--features-cnt-ttl');
    var $elem05 = $('.p-service--points-cnt-ttl');
    var $elem06 = $('.p-top--news-item');
    if ($elem01.length) {
      $elem01.matchHeight();
    }
    if ($elem02.length) {
      $elem02.matchHeight();
    }
    if ($elem03.length) {
      $elem03.matchHeight();
    }
    if ($elem04.length) {
      $elem04.matchHeight();
    }
    if ($elem05.length) {
      $elem05.matchHeight();
    }
    if ($elem06.length) {
      $elem06.matchHeight();
    }
  }

  /* --------------------
  INIT (WINDOW ON LOAD)
  --------------------- */
  // Run all script when DOM has loaded
  var init = function() {
    objectFitImages();
    triggerMenu();
    anchorLink();
    scrollLoad();
    initSliders();
    clickBacktop();
    triggerBackground();
    tabsControl();
    triggerAccordion();
    matchHeight();
  }

  init();


  /* --------------------
  WINDOW ON RESIZE
  --------------------- */
  $(window).resize(function() {
    wWindow = $(window).outerWidth();
  });


  /* --------------------
  WINDOW ON SCROLL
  --------------------- */
  $(window).scroll(function() {
    scrollLoad();
    triggerBacktop();
    triggerBackground();
  });

});
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/variables.php'; ?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>QQ technology | Santa Mineral</title>
  <meta name="description" content="">
  <meta property="og:site_name" content="">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:type" content="website">
  <meta property="og:url" content="">
  <meta property="og:image" content="">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:image" content="">
  <!-- <link rel="shortcut icon" href="assets/images/favicon.png">
  <link rel="apple-touch-icon" href="assets/images/favicon.png"> -->
  <link rel="stylesheet" href="<?php echo $PATH;?>/assets/css/index.css">
</head>

<body>
  <div class="wrapper">
    <header class="header js-header">
      <h1 class="header-logo link">
        <a href="/"><img src="<?php echo $PATH;?>/assets/images/common/logo-header.png" alt="Santa Mineral （サンタミネラル）"></a>
      </h1>
      <div class="header-menu js-menu">
        <div class="top-navWrap">
          <ul class="top-nav">
            <li><a class="link" href="/about">当社について</a></li>
            <li><a class="link" href="/technology">QQ TECHNOLOGYとは</a></li>
            <li><a class="link" href="/works">技術・製品情報</a></li>
            <li><a class="link" href="/faq">よくあるご質問</a></li>
          </ul>
          <div class="bogo-language-switcherWrap">
            <p class="bogo-language-switcher-label">LANGUAGE</p>
            <ul class="bogo-language-switcher">
              <li class="on">
                <a href="">
                  <img src="<?php echo $PATH;?>/assets/images/common/flag01.svg" alt="">
                </a>
              </li>
              <li>
                <a href="">
                  <img src="<?php echo $PATH;?>/assets/images/common/flag02.svg" alt="">
                </a>
              </li>
              <li>
                <a href="">
                  <img src="<?php echo $PATH;?>/assets/images/common/flag03.svg" alt="">
                </a>
              </li>
              <li>
                <a href="">
                  <img src="<?php echo $PATH;?>/assets/images/common/flag04.svg" alt="">
                </a>
              </li>
            </ul>
          </div>
        </div>
        <a href="/contact" class="header-menu--contact">お問い合わせ</a>
        <div class="header-access sp-only">
          <div class="header-access--logo">
            <a class="link" href="/"><img src="<?php echo $PATH;?>/assets/images/common/logo-header.png" alt=""></a>
          </div>
          <p class="header-access--address">〒105-0013 東京都港区浜松町二丁目6番4号ガリシア浜松町1401号</p>
          <a class="header-access--direct viewmore" href="" target="_blank">Google Map</a>
        </div>
      </div>
      <a class="header-ctrl js-menuTrigger" href="javascript:void(0);">
        <span></span>
        <span></span>
        <span></span>
      </a>
      <div class="header-menu--overlay js-menuOverlay sp-only"></div>
    </header><!-- ./header -->
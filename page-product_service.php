<?php /* Template Name: ＱＱサービス */ 
get_header(); ?>

<main class="main p-end">
    <div class="container type2">
        <div class="breadcrumbWrap">
            <div class="breadcrumb">
                <?php
                if($locale == 'ja') {
                    $txt_home = 'トップページ';
                } elseif ($locale == 'en_US') {
                    $txt_home = 'Top Page';
                } elseif ($locale == 'zh_CN') {
                    $txt_home = 'Top Page';
                }
                ?>
                <ul>
                    <li><a href="<?php echo home_url()?>"><?php echo $txt_home?></a></li>
                    <?php 
                    if($locale == 'ja') {
                        printf('<li><a href="%s">%s</a></li>', home_url('service-product'), '技術・製品情報');
                    } elseif ($locale == 'en_US') {
                        printf('<li><a href="%s">%s</a></li>', home_url('service-product'), 'Technology / product introduction');
                    } elseif ($locale == 'zh_CN') {
                        printf('<li><a href="%s">%s</a></li>', home_url('service-product'), '技術・產品介紹');
                    }
                    ?>
                    <li><?php the_title();?></li>
                </ul>
            </div>
        </div><!-- ./breadcrumbWrap -->

        <section class="p-end--banner type2">
            <h1 class="p-end--ttl">
                <img src="<?php the_field('title_en'); ?>" alt="">
                <span><?php the_title() ?></span>
            </h1>
        </section><!-- ./p-recruit--banner -->

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
        
                <div class="p-service--cnt">
                    <div class="p-service--detail">
                        <div class="p-service--detail-infor">
                            <?php
                                if (have_rows('qq_service_features')): while (have_rows('qq_service_features')) : the_row();?>
                                <div class="p-service--detail-row">
                                    <?php if(get_sub_field('heading')){?>
                                        <h4 class="title-lv3"><?php the_sub_field('heading')?></h4>
                                    <?php }?>
                                    <?php
                                    if (have_rows('items')) { $j=0;
                                        echo '<ul class="p-service--features">';
                                        while (have_rows('items')) : $j++; the_row();?>
                                            <li class="p-service--features-item">
                                                <div class="p-service--features-ttlWrap">
                                                    <div class="p-service--features-numberWrap">
                                                        <span class="p-service--features-label stroke-white">FEATURES</span>
                                                        <span class="p-service--features-number stroke-blue"><?php echo $j;?></span>
                                                    </div>
                                                    <h5 class="p-service--features-item-ttl"><?php the_sub_field('heading2')?></h5>
                                                </div>
                                                <div class="p-service--features-cnt">
                                                    <p class="p-service--features-cnt-ttl"><?php the_sub_field('desc')?></p>
                                                </div>
                                            </li>
                                            <?php
                                        endwhile;
                                        echo '</ul>';
                                    }
                                    ?>
                                </div><!-- ./p-service--detail-row -->
                                <?php endwhile; ?>
                            <?php endif; ?>

                            <?php
                                if (have_rows('qq_service_features2')): while (have_rows('qq_service_features2')) : the_row();?>
                                <div class="p-service--detail-row">
                                    <?php if(get_sub_field('heading')){?>
                                        <h4 class="title-lv3"><?php the_sub_field('heading')?></h4>
                                    <?php }?>
                                    <?php
                                    if (have_rows('items')) { $j=0;
                                        echo '<ul class="p-service--features">';
                                        while (have_rows('items')) : $j++; the_row();?>
                                            <li class="p-service--features-item">
                                                <div class="p-service--features-ttlWrap">
                                                    <h5 class="p-service--features-item-ttl"><?php the_sub_field('heading')?></h5>
                                                    <div class="p-service--features-item-thumb">
                                                        <img src="<?php the_sub_field('icon')?>">
                                                    </div>
                                                </div>
                                                <div class="p-service--features-cnt">
                                                    <p class="p-service--features-cnt-ttl"><?php the_sub_field('content')?></p>
                                                </div>
                                            </li>
                                            <?php
                                        endwhile;
                                        echo '</ul>';
                                    }
                                    ?>
                                </div><!-- ./p-service--detail-row -->
                                <?php endwhile; ?>
                            <?php endif; ?>

                            <?php if (have_rows('qq_service_desc')): while (have_rows('qq_service_desc')) : the_row();?>
                                <div class="p-service--detail-row">
                                    <?php if(get_sub_field('heading')){?>
                                        <h4 class="title-lv3"><?php the_sub_field('heading')?></h4>
                                    <?php }?>
                                    <?php
                                    if (have_rows('items')) {
                                        while (have_rows('items')) : the_row();?>
                                            <h4 class="title-lv3"><?php the_sub_field('title')?></h4>
                                            <div class="thumb-wrapper">
                                                <div class="col2-75--left">
                                                    <p class="desc2 mgb-10"><?php echo nl2br(get_sub_field('desc'))?></p>
                                                </div>
                                                <?php if(get_sub_field('image')) {?>
                                                <div class="align-center">
                                                    <img src="<?php the_sub_field('image')?>" alt="">
                                                </div>
                                                <?php }?>
                                            </div>
                                            <?php
                                        endwhile;
                                    }
                                    ?>
                                </div><!-- ./p-service--detail-row -->
                                <?php endwhile; ?>
                            <?php endif; ?>

                            <?php
                                if (have_rows('qq_service_case')): while (have_rows('qq_service_case')) : the_row();?>
                                <div class="p-service--detail-row">
                                    <?php if(get_sub_field('heading')){?>
                                        <h4 class="title-lv3"><?php the_sub_field('heading')?></h4>
                                    <?php }?>
                                    <?php
                                    if (have_rows('items')) {
                                        echo '<div class="p-service--products">';
                                        echo '<ul class="p-service--products-list mgb-80">';
                                        while (have_rows('items')) : the_row();?>
                                            <li class="p-service--products-item">
                                                <div class="p-service--products-item-inner">
                                                    <?php if(get_sub_field('image')) {?>
                                                    <div class="p-service--products-item-thumb">
                                                        <img class="cover" src="<?php the_sub_field('image')?>" alt="">
                                                    </div>
                                                    <?php }?>
                                                    <div class="p-service--products-item-cnt type2">
                                                        <div class="p-service--products-item-infor">
                                                            <h4 class="title-lv4"><?php the_sub_field('title')?></h4>
                                                            <p class="desc2"><?php echo nl2br(get_sub_field('desc'))?></p>
                                                        </div>
                                                        <?php if(get_sub_field('link')) {?>
                                                        <div class="link-borderWrap">
                                                            <a href="<?php echo get_permalink(get_sub_field('link'))?>" class="link-border">
                                                                <span>
                                                                    <?php
                                                                    if ($locale == 'ja') {
                                                                        print '詳しくみる';
                                                                    } elseif ($locale == 'en_US') {
                                                                        print 'Click here for more info';
                                                                    } elseif ($locale == 'zh_CN') {
                                                                        print 'Click here for more info';
                                                                    }
                                                                    ?>
                                                                </span>
                                                            </a>
                                                        </div>
                                                        <?php }?>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php
                                        endwhile;
                                        echo '</ul>';
                                        echo '</div>';
                                    }
                                    ?>
                                </div><!-- ./p-service--detail-row -->
                                <?php endwhile; ?>
                            <?php endif; ?>

                            <?php if (have_rows('qq_service_result')): while (have_rows('qq_service_result')) : the_row();?>
                                <div class="p-service--detail-row">
                                    <?php if(get_sub_field('heading')){?>
                                        <h4 class="title-lv3"><?php the_sub_field('heading')?></h4>
                                    <?php }?>
                                    <p class="desc2 mgb-20"><?php echo nl2br(get_sub_field('desc'))?></p>
                                    <?php if (have_rows('items')) {?>
                                        <div class="table custom-width01">
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th><?php echo do_shortcode('[ja]試験内容(報告日)[/ja][en]Content of test[/en][zh]實驗內容[/zh]')?></th>
                                                        <th><?php echo do_shortcode('[ja]評価[/ja][en]Evaluation[/en][zh]評價[/zh]')?></th>
                                                        <th><?php echo do_shortcode('[ja]試験結果[/ja][en]Test result[/en][zh]試驗結果[/zh]')?></th>
                                                        <th><?php echo do_shortcode('[ja]検査機関[/ja][en]Testing institution[/en][zh]試驗機關[/zh]')?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if (have_rows('items')) {
                                                        while (have_rows('items')) : the_row();
                                                            $noi_dung = get_sub_field('noi_dung');
                                                            ?>
                                                            <tr>
                                                                <td data-label="<?php echo do_shortcode('[ja]試験内容(報告日)[/ja][en]Content of test[/en][zh]實驗內容[/zh]')?>">
                                                                    <a href="<?php echo $noi_dung['file']?>" target="_blank" class="link-icon pdf"><?php echo $noi_dung['file_name']?></a>
                                                                    <p class="desc2"><?php echo $noi_dung['thoi_gian']?></p>
                                                                </td>
                                                                <td data-label="<?php echo do_shortcode('[ja]評価[/ja][en]Evaluation[/en][zh]評價[/zh]')?>"><?php the_sub_field('danh_gia')?></td>
                                                                <td data-label="<?php echo do_shortcode('[ja]試験結果[/ja][en]Test result[/en][zh]試驗結果[/zh]')?>"><?php echo nl2br(get_sub_field('ket_qua'))?></td>
                                                                <td data-label="<?php echo do_shortcode('[ja]検査機関[/ja][en]Testing institution[/en][zh]試驗機關[/zh]')?>"><?php the_sub_field('kensa_kikan')?></td>
                                                            </tr>
                                                            <?php
                                                        endwhile;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php }?>
                                </div><!-- ./p-service--detail-row -->
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div><!-- ./p-service--detail-infor   -->
                    </div><!-- ./p-service--detail -->
                    <div class="p-service--infor">
                        <?php the_field('qq_service_info')?>
                    </div><!-- ./p-service--infor -->
                </div><!-- ./p-service--cnt -->
            <?php endwhile; ?>
        <?php endif; ?>            
    </div>
    <div class="align-center mgt-60">
        <?php 
        if($locale == 'ja') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url('product'), '技術・製品紹介に戻る');
        } elseif ($locale == 'en_US') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url('product'), 'Technology / product introduction');
        } elseif ($locale == 'zh_CN') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url('product'), '技術・產品介紹');
        }
        ?>
    </div>
</main>

<?php get_footer(); ?>

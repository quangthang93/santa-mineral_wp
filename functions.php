<?php
define('THEME_NAME', 'demo21');

$locale = get_locale();

if (function_exists('add_theme_support')) {
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_theme_support('automatic-feed-links');
}

/* ------------------------------------*\
  Functions
  \*------------------------------------ */

function main_nav() {
    wp_nav_menu(
            array(
                'theme_location' => 'header-menu',
                'menu' => '',
                'container' => FALSE,
                'container_class' => 'menu-{menu slug}-container',
                'container_id' => '',
                'menu_class' => '',
                'menu_id' => '',
                'echo' => true,
                'fallback_cb' => '',
                'link_before' => '',
                'link_after' => '',
                'items_wrap' => '<ul class="top-nav">%3$s</ul>',
                'depth' => 0,
                'walker' => ''
            )
    );
}

function footer_nav() {
    wp_nav_menu(
            array(
                'theme_location' => 'footer-menu',
                'menu' => '',
                'container' => FALSE,
                'container_class' => 'menu-{menu slug}-container',
                'container_id' => '',
                'menu_class' => '',
                'menu_id' => '',
                'echo' => true,
                'fallback_cb' => '',
                'link_before' => '',
                'link_after' => '',
                'items_wrap' => '<ul class="footer-head--nav">%3$s</ul>',
                'depth' => 0,
                'walker' => ''
            )
    );
}

//Remove JQuery migrate
function remove_jquery_migrate($scripts) {
    if (!is_admin() && isset($scripts->registered['jquery'])) {
        $script = $scripts->registered['jquery'];

        if ($script->deps) { // Check whether the script has any dependencies
            $script->deps = array_diff($script->deps, array(
                'jquery-migrate'
            ));
        }
    }
}

add_action('wp_default_scripts', 'remove_jquery_migrate');

//Upgrade jQuery version
function sof_load_scripts() {
    if (!is_admin()) {
        wp_deregister_script('jquery');
        wp_register_script('jquery', 'https://code.jquery.com/jquery-3.4.1.min.js', '3.4.1', false);
        wp_enqueue_script('jquery');
    }
}
//WPの最新バージョンにて一、jquery最新バージョンが使われているので旦停止になる。
//add_action('wp_enqueue_scripts', 'sof_load_scripts');

function load_styles() {
    wp_enqueue_style('style', get_stylesheet_uri());

    wp_enqueue_style('index_css', get_template_directory_uri() . "/assets/css/index.css");
    wp_enqueue_style('custom_css', get_template_directory_uri() . '/css/custom.css');
}

function load_scripts() {
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {       
        wp_enqueue_script('js_ofi', get_template_directory_uri() . '/assets/js/libs/ofi.min.js', array('jquery'));
        wp_enqueue_script('js_slick', get_template_directory_uri() . '/assets/js/libs/slick.min.js');
        wp_enqueue_script('js_matchHeight', get_template_directory_uri() . '/assets/js/libs/jquery.matchHeight-min.js');
        wp_enqueue_script('js_common', get_template_directory_uri() . '/assets/js/common.js', '', '', true);
        wp_register_script('ajaxzip3', 'https://ajaxzip3.github.io/ajaxzip3.js', array('jquery'), '', true);
        wp_enqueue_script('custom_js', get_template_directory_uri() . '/js/custom.js', array('jquery'), '', true);        
        
        if (is_page(2) || is_page(942) || is_page(943)):
            wp_enqueue_script('ajaxzip3');
        endif;
    }
}

function register_site_menu() {
    register_nav_menus(array(
        'header-menu' => __('Main Menu', THEME_NAME),
        'footer-menu' => __('Footer Menu', THEME_NAME)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '') {
    $args['container'] = false;
    return $args;
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist) {
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class
function add_slug_to_body_class($classes) {
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag) {
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions($html) {
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

/* ------------------------------------*\
  Actions + Filters + ShortCodes
  \*------------------------------------ */

// Add Actions
add_action('wp_enqueue_scripts', 'load_styles');
add_action('wp_enqueue_scripts', 'load_scripts');
add_action('init', 'register_site_menu');
// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// Remove jQuery Migrate Script from header and Load jQuery from Google API
function remove_wp_embed_and_jquery() {
    if (!is_admin()) {
        wp_deregister_script('wp-embed');
    }
}

// add_action('init', 'remove_wp_embed_and_jquery');

// Add Filters
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images
// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

function remove_api() {
    remove_action('wp_head', 'rest_output_link_wp_head', 10);
    remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
}

// add_action('after_setup_theme', 'remove_api');

//Remove <link rel='dns-prefetch' href='//s.w.org'> on header
remove_action('wp_head', 'wp_resource_hints', 2);

if (!function_exists('wp_breadcrumb')) {
    function wp_breadcrumb($list_style = 'ul', $list_id = '', $list_class = '', $active_class = 'active', $echo = true) {
        // Open list
        global $locale;
        
        $breadcrumb = '<' . $list_style . ' id="' . $list_id . '" class="' . $list_class . '">';

        $breadcrumb .= '<li><a href="' . home_url() . '">'.do_shortcode('[ja]トップページ[/ja][en]Top Page[/en][zh]Top Page[/zh]').'</a></li>';

        // Blog archive
        if ('page' == get_option('show_on_front') && get_option('page_for_posts')) {
            $blog_page_id = get_option('page_for_posts');
            if (is_home()) {
                $breadcrumb .= '<li class="' . $active_class . '">' . get_the_title($blog_page_id) . '</li>';
            } else if (is_category() || is_tag() || is_author() || is_date() || is_singular('post')) {
                $breadcrumb .= '<li><a href="' . get_permalink($blog_page_id) . '">' . get_the_title($blog_page_id) . '</a></li>';
            }
        }
        // Category, tag, author and date archives
        if (is_archive() && !is_tax() && !is_post_type_archive()) {
            $breadcrumb .= '<li class="' . $active_class . '">';
            // Title of archive
            if (is_category()) {
                $breadcrumb .= single_cat_title('', false);
            } else if (is_tag()) {
                $breadcrumb .= single_tag_title('', false);
            } else if (is_author()) {
                $breadcrumb .= get_the_author();
            } else if (is_date()) {
                if (is_day()) {
                    $breadcrumb .= get_the_time('F j, Y');
                } else if (is_month()) {
                    $breadcrumb .= get_the_time('F, Y');
                } else if (is_year()) {
                    $breadcrumb .= get_the_time('Y');
                }
            }
            $breadcrumb .= '</li>';
        }
        // Posts
        if (is_singular('post')) {
            // Post categories
            $post_cats = get_the_category();
            if ($post_cats[0]) {
                $breadcrumb .= '<li><a href="' . get_category_link($post_cats[0]->term_id) . '">' . $post_cats[0]->name . '</a></li>';
            }
            // Post title
            $breadcrumb .= '<li class="' . $active_class . '">' . get_the_title() . '</li>';
        }
        // Pages
        if (is_page() && !is_front_page()) {
            $post = get_post(get_the_ID());
            // Page parents
            if ($post->post_parent) {
                $parent_id = $post->post_parent;
                $crumbs = array();
                while ($parent_id) {
                    $page = get_page($parent_id);
                    $crumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
                    $parent_id = $page->post_parent;
                }
                $crumbs = array_reverse($crumbs);
                foreach ($crumbs as $crumb) {
                    $breadcrumb .= '<li>' . $crumb . '</li>';
                }
            }

            // Page title
            $breadcrumb .= '<li class="' . $active_class . '">' . get_the_title() . '</li>';
        }
        // Attachments
        if (is_attachment()) {
            // Attachment parent
            $post = get_post(get_the_ID());
            if ($post->post_parent) {
                $breadcrumb .= '<li><a href="' . get_permalink($post->post_parent) . '">' . get_the_title($post->post_parent) . '</a></li>';
            }
            // Attachment title
            $breadcrumb .= '<li class="' . $active_class . '">' . get_the_title() . '</li>';
        }
        // Search
        if (is_search()) {
            $breadcrumb .= '';
        }
        // 404
        if (is_404()) {
            $breadcrumb .= '<li class="' . $active_class . '">' . __('404', THEME_NAME) . '</li>';
        }
        // Custom Post Type Archive
        if (is_post_type_archive()) {
            if (!is_search()) {
                $breadcrumb .= '<li class="' . $active_class . '">' . post_type_archive_title('', false) . '</li>';
            }
        }
        // Custom Taxonomies
        if (is_tax()) {
            // Get the post types the taxonomy is attached to
            $current_term = get_queried_object();
            $attached_to = array();
            $post_types = get_post_types();
            foreach ($post_types as $post_type) {
                $taxonomies = get_object_taxonomies($post_type);
                if (in_array($current_term->taxonomy, $taxonomies)) {
                    $attached_to[] = $post_type;
                }
            }
            // Post type archive link
            $output = false;
            foreach ($attached_to as $post_type) {
                $cpt_obj = get_post_type_object($post_type);
                if (!$output && get_post_type_archive_link($cpt_obj->name)) {
                    $breadcrumb .= '<li><a href="' . get_post_type_archive_link($cpt_obj->name) . '">' . $cpt_obj->labels->name . '</a></li>';
                    $output = true;
                }
            }
            // Term title
            $breadcrumb .= '<li class="' . $active_class . '">' . single_term_title('', false) . '</li>';
        }
        // Custom Post Types
        if (is_single() && get_post_type() != 'post' && get_post_type() != 'attachment') {
            $cpt_obj = get_post_type_object(get_post_type());
            // Is cpt hierarchical like pages or posts?
            if (is_post_type_hierarchical($cpt_obj->name)) {
                // Like pages
                // Cpt archive
                if (get_post_type_archive_link($cpt_obj->name)) {
                    $breadcrumb .= '<li><a href="' . get_post_type_archive_link($cpt_obj->name) . '">' . $cpt_obj->labels->name . '</a></li>';
                }
                // Cpt parents
                $post = get_post(get_the_ID());
                if ($post->post_parent) {
                    $parent_id = $post->post_parent;
                    $crumbs = array();
                    while ($parent_id) {
                        $page = get_page($parent_id);
                        $crumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
                        $parent_id = $page->post_parent;
                    }
                    $crumbs = array_reverse($crumbs);
                    foreach ($crumbs as $crumb) {
                        $breadcrumb .= '<li>' . $crumb . '</li>';
                    }
                }
            } else {
                // Like posts
                // Cpt archive
                if (get_post_type_archive_link($cpt_obj->name)) {
                    $breadcrumb .= '<li><a href="' . get_post_type_archive_link($cpt_obj->name) . '">' . $cpt_obj->labels->name . '</a></li>';
                }
                // Get cpt taxonomies
                $cpt_taxes = get_object_taxonomies($cpt_obj->name);
                if ($cpt_taxes && is_taxonomy_hierarchical($cpt_taxes[0])) {
                    // Other taxes attached to the cpt could be hierachical, so need to look into that.
                    $cpt_terms = get_the_terms(get_the_ID(), $cpt_taxes[0]);
                    if (is_array($cpt_terms)) {
                        $output = false;
                        foreach ($cpt_terms as $cpt_term) {
                            if (!$output) {
//                                $breadcrumb .= '<div class="c-breadcrumbs__li"><a href="' . get_term_link($cpt_term, $cpt_taxes[0]) . '">' . $cpt_term->name . '</a></div>';
                                $output = true;
                            }
                        }
                    }
                }
            }
            // Cpt title
            $breadcrumb .= '<li class="' . $active_class . '">' . get_the_title() . '</li>';
        }
        // Close list
        $breadcrumb .= '</' . $list_style . '>';
        // Ouput
        if ($echo) {
            echo $breadcrumb;
        } else {
            return $breadcrumb;
        }
    }

}

function cut_title($text, $len = 30) { //Hàm cắt tiêu đề Unicode
    mb_internal_encoding('UTF-8');
    if ((mb_strlen($text, 'UTF-8') > $len))
        $text = mb_substr($text, 0, $len, 'UTF-8') . "...";
    return $text;
}

function cut_str($text, $limit = 25) {
    $more = (mb_strlen($text) > $limit) ? TRUE : FALSE;
    $text = mb_substr($text, 0, $limit, 'UTF-8');
    return array($text, $more);
}

function pagination($query_data = '', $range = 4, $num_stt = 12) {
    $showitems = ($range * 2) + 1;

    global $paged, $locale;
    if (empty($paged))
        $paged = 1;

    if (empty($query_data) || !is_object($query_data)) {
        global $wp_query;
        $query_data = $wp_query;
    }
    $pages = $query_data->max_num_pages;

    if (1 != $pages) {
        $my_posts_per_page = $num_stt;  //1ページに表示する最大投稿数
        $my_post_range = ($paged - 1) * $my_posts_per_page + 1;
        switch ($paged) {
            case 1:
                $pos_count = $query_data->post_count;
                break;
            case $paged == $query_data->max_num_pages:
                $pos_count = $query_data->found_posts;
                break;
            default:
                $pos_count = $my_post_range + $num_stt;
                break;
        }

        echo "<div class='pagination'>";
        if($locale == 'ja') {
            echo "<p class=\"pagination-label\">" . $query_data->found_posts . "件中｜{$my_post_range}〜" . $pos_count . "件 表示</p>";
        } elseif ($locale == 'en_US') {
            echo "<p class=\"pagination-label\">Page {$my_post_range}〜" . $pos_count . " of ".$query_data->found_posts."</p>";
        } elseif ($locale == 'zh_CN') {
            echo "<p class=\"pagination-label\">Page {$my_post_range}〜" . $pos_count . " of ".$query_data->found_posts."</p>";
        }
        echo "<div class=\"pagination-list\">";
        if ($paged > 2 && $paged > $range + 1 && $showitems < $pages)
            echo "<a href='" . get_pagenum_link(1) . "'>&laquo;</a>";
        if ($paged > 1 && $showitems < $pages)
            echo "<a href='" . get_pagenum_link($paged - 1) . "'>&lsaquo;</a>";

        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems )) {
                echo ($paged == $i) ? "<a class=\"active\">" . $i . "</a>" : "<a href='" . get_pagenum_link($i) . "'>" . $i . "</a>";
            }
        }

        if ($paged < $pages && $showitems < $pages)
            echo "<a href=\"" . get_pagenum_link($paged + 1) . "\">&rsaquo;</a>";
        if ($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages)
            echo "<a href='" . get_pagenum_link($pages) . "'>&raquo;</a>";
        echo "</div>";
        echo "</div>";
    }
}

// Set path relative of image
add_filter('image_send_to_editor', 'image_to_relative', 5, 8);

function image_to_relative($html, $id, $caption, $title, $align, $url, $size, $alt) {
    $sp = strpos($html, "src=") + 5;
    $ep = strpos($html, "\"", $sp);

    $imageurl = substr($html, $sp, $ep - $sp);

    $relativeurl = str_replace("http://", "", $imageurl);
    $sp = strpos($relativeurl, "/");
    $relativeurl = substr($relativeurl, $sp);

    $html = str_replace($imageurl, $relativeurl, $html);

    return $html;
}

//Xoa width shortcode mac dinh caption
add_shortcode('wp_caption', 'fixed_img_caption_shortcode');
add_shortcode('caption', 'fixed_img_caption_shortcode');

function fixed_img_caption_shortcode($attr, $content = null) {
    // New-style shortcode with the caption inside the shortcode with the link and image tags.
    if (!isset($attr['caption'])) {
        if (preg_match('#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches)) {
            $content = $matches[1];
            $attr['caption'] = trim($matches[2]);
        }
    }
    // Allow plugins/themes to override the default caption template.
    $output = apply_filters('img_caption_shortcode', '', $attr, $content);
    if ($output != '')
        return $output;
    extract(shortcode_atts(array(
        'id' => '',
        'align' => 'alignnone',
        'width' => '',
        'caption' => ''
                    ), $attr));
    if (1 > (int) $width || empty($caption))
        return $content;
    if ($id)
        $id = 'id="' . esc_attr($id) . '" ';
    return '<div ' . $id . 'class="wp-caption ' . esc_attr($align) . '">'
            . do_shortcode($content) . '<p class="wp-caption-text">' . $caption . '</p></div>';
}

//Change show default of tag
add_filter('get_terms_args', 'editor_show_tags');

function editor_show_tags($args) {
    if (defined('DOING_AJAX') && DOING_AJAX && isset($_POST['action']) && $_POST['action'] === 'get-tagcloud') {
        unset($args['number']);
        $args['hide_empty'] = 0;
    }
    return $args;
}

//Disble srcset
add_filter('wp_calculate_image_srcset', '__return_false');

//Keep classic editor
add_filter('use_block_editor_for_post', '__return_false');

//Disable Gutenberg stylesheet
function wps_deregister_styles() {
    wp_dequeue_style('wp-block-library');
    wp_dequeue_style('wp-block-library-theme');
}

add_action('wp_print_styles', 'wps_deregister_styles', 100);

// Disable update emails
add_filter('auto_core_update_send_email', '__return_true');

function mwform_validation_rule($Validation, $data) {
    $validation_select_msg = '未選択です。';
    $validation_input_msg = '未入力です。';
    $validation_chk_msg = 'チェックがされていません。';

    if (empty($data['type1'])) {
        $Validation->set_rule('type1', 'required', array('message' => $validation_select_msg));
    }
    if (empty($data['product'])) {
        $Validation->set_rule('product', 'noempty', array('message' => $validation_select_msg));
    }
    if (empty($data['employment'])) {
        $Validation->set_rule('employment', 'noempty', array('message' => $validation_select_msg));
    }
    if (empty($data['address1'])) {
        $Validation->set_rule('address1', 'noempty', array('message' => $validation_input_msg));
    }
    $Validation->set_rule('privacy', 'required', array('message' => $validation_chk_msg));    
        
    if($data['type1'] === '法人') {
        $Validation->set_rule('company', 'noempty');
    }
    return $Validation;
}

add_filter('mwform_validation_mw-wp-form-215', 'mwform_validation_rule', 10, 2);

function ajaxzip() {    
    if (is_page(2) || is_page(942) || is_page(943)):
        ?>
        <script>
            jQuery(function($){
                $(document).on('change', '#zip', function(e) {
                    AjaxZip3.zip2addr(this, null, 'pref', 'address');
                    return false;
                });
            });
        </script>
        <?php
    endif;  
}
add_action('wp_footer', 'ajaxzip', 100);

function func_company_patent($atts) {
    ob_start();
    extract(shortcode_atts(array(
        'item' => -1,
                    ), $atts));

    global $locale;
    
    if( have_rows('patent_items') ): ?>
    <div class="p-company--table table custom-width02">
        <table>
            <thead>
                <tr>
                    <th><?php echo do_shortcode('[ja]特許番号（出願日）[/ja][en]Patent Number(Apply date)[/en][zh]專利編號(申請日）[/zh]') ?></th>
                    <th><?php echo do_shortcode('[ja]権利国[/ja][en]Rights by country[/en][zh]權利國[/zh]') ?></th>
                    <th><?php echo do_shortcode('[ja]発明の名称[/ja][en]Name of innovation[/en][zh]發明的名稱[/zh]') ?></th>
                    <th><?php echo do_shortcode('[ja]発明者[/ja][en]Innovator[/en][zh]發明者[/zh]') ?></th>
                    <th><?php echo do_shortcode('[ja]特許権者[/ja][en]Owner of patent[/en][zh]專利權者[/zh]') ?></th>
                    <th><?php echo do_shortcode('[ja]ステータス[/ja][en]Status[/en][zh]狀態[/zh]') ?></th>
                </tr>
            </thead>
            <?php while( have_rows('patent_items') ): the_row();
                $status = get_sub_field('status');
                ?>
                <tr>
                    <td data-label="<?php echo do_shortcode('[ja]特許番号（出願日）[/ja][en]Patent Number(Apply date)[/en][zh]專利編號(申請日）[/zh]') ?>"><?php the_sub_field('stt')?><?php get_sub_field('time') ? printf('<br>(%s)', get_sub_field('time')) : '';?></td>
                    <td data-label="<?php echo do_shortcode('[ja]権利国[/ja][en]Rights by country[/en][zh]權利國[/zh]') ?>"><?php the_sub_field('kenri')?></td>
                    <td data-label="<?php echo do_shortcode('[ja]発明の名称[/ja][en]Name of innovation[/en][zh]發明的名稱[/zh]') ?>"><?php the_sub_field('invention')?></td>
                    <td data-label="<?php echo do_shortcode('[ja]発明者[/ja][en]Innovator[/en][zh]發明者[/zh]') ?>"><?php the_sub_field('inventor')?></td>
                    <td data-label="<?php echo do_shortcode('[ja]特許権者[/ja][en]Owner of patent[/en][zh]專利權者[/zh]') ?>"><?php the_sub_field('owner')?></td>
                    <td data-label="<?php echo do_shortcode('[ja]ステータス[/ja][en]Status[/en][zh]狀態[/zh]') ?>">
                        <?php if($status['file']) {?>
                            <a href="<?php echo $status['file']?>" target="_blank" class="link-icon pdf"><?php echo $status['name']?></a>
                        <?php } else {?>
                            <?php echo $status['name']?>
                        <?php }?>
                    </td>
                </tr>
            <?php endwhile; ?>
        </table>
    </div>
<?php endif;

    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}

add_shortcode('patent', 'func_company_patent');

function func_company_overview($atts) {
    ob_start();
    extract(shortcode_atts(array(
        'item' => -1,
                    ), $atts));

    global $post;?>
    
    <div class="p-company--infor">
        <ul class="anchor--list">
            <li>
                <a href="#profile" class="link-anchor">会社概要</a>
            </li>
            <li>
                <a href="#listOfBases" class="link-anchor">拠点一覧</a>
            </li>
        </ul><!-- .p-company--infor-bar -->
        <div class="p-company--infor-cnt">
            <?php if (have_rows('company_ovierview')): ?>
                <h2 class="title-lv2 mgb-60" id="profile">会社概要</h2>
                <div class="table">
                    <table class="mgb-100">

                        <?php while (have_rows('company_ovierview')): the_row();
                            ?>
                            <tr>
                                <th><?php the_sub_field('title') ?></th>
                                <td><?php echo nl2br(the_sub_field('content')) ?></td>                    
                            </tr>
                        <?php endwhile; ?>
                    </table>
                </div>
            <?php endif; ?>
            <h2 class="title-lv2 mgb-60" id="listOfBases">拠点一覧</h2>
            <?php the_field('company_branch')?>
        </div>
    </div><!-- .p-company--infor -->
<?php
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}

add_shortcode('company_overview', 'func_company_overview');

function cptui_register_my_taxes_category_news() {

	/**
	 * Taxonomy: カテゴリー.
	 */

	$labels = [
		"name" => __( "カテゴリー", "custom-post-type-ui" ),
		"singular_name" => __( "カテゴリー", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "カテゴリー", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'news', 'with_front' => true,  'hierarchical' => true, ],
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "category_news",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
			];
	register_taxonomy( "category_news", [ "news" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes_category_news' );

function cptui_register_my_taxes_category_product() {

	/**
	 * Taxonomy: カテゴリー.
	 */

	$labels = [
		"name" => __( "カテゴリー", "custom-post-type-ui" ),
		"singular_name" => __( "カテゴリー", "custom-post-type-ui" ),
	];

	
	$args = [
		"label" => __( "カテゴリー", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'category_product', 'with_front' => true,  'hierarchical' => true, ],
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "category_product",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
		"show_in_graphql" => false,
	];
	register_taxonomy( "category_product", [ "product" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes_category_product' );

add_shortcode('path_theme', 'shortcode_tp');

function shortcode_tp() {
    return get_template_directory_uri();
}

/**
 * 多言語対応する投稿タイプ追加
 *
 * @param array $localizable 多言語対応する投稿タイプ.
 *
 * @return array
 */
function my_localizable_post_types( $localizable ) {
	$custom_post_types = ['product', 'faq', 'news'];
    return array_merge($localizable,$custom_post_types);
}
add_filter( 'bogo_localizable_post_types', 'my_localizable_post_types', 10, 1 );

// Bogo 国旗アイコンを削除
function bogo_use_flags_false() {
    return false;
}

add_filter('bogo_use_flags', 'bogo_use_flags_false');

add_filter('bogo_language_switcher', 'replace_bogo_text');

function replace_bogo_text($output) {
    $output = str_replace('English', 'ENG', $output);
    
    return $output;
}

function ignore_shortcode($atts, $content = null) {
    return null;
}

function show_shortcode($atts, $content = null) {
    return $content;
}

if ($locale == 'ja') {
    add_shortcode('en', 'ignore_shortcode');
    add_shortcode('ja', 'show_shortcode');
    add_shortcode('zh', 'ignore_shortcode');
} elseif ($locale == 'en_US') {
    add_shortcode('en', 'show_shortcode');
    add_shortcode('ja', 'ignore_shortcode');
    add_shortcode('zh', 'ignore_shortcode');
} elseif ($locale == 'zh_CN') {
    add_shortcode('zh', 'show_shortcode');
    add_shortcode('ja', 'ignore_shortcode');
    add_shortcode('en', 'ignore_shortcode');
}

add_action('wp_head', 'func_404_redirect');

function func_404_redirect() {
    if(is_404()) {
        printf('<meta http-equiv="refresh" content="3; URL=%s">', home_url());
    }
}
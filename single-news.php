<?php get_header(); ?>

<main class="main p-end">

    <div class="p-end--cnt">
        <div class="container">
            <div class="breadcrumbWrap">
                <div class="breadcrumb">
                    <?php wp_breadcrumb() ?>
                </div>
            </div><!-- ./breadcrumbWrap -->
            <section class="p-end--banner type2">
                <h1 class="p-end--ttl">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/end/ttl-news.png" alt="">
                    <span><?php echo do_shortcode('[ja]ニュース・レポート[/ja][en]News & Report[/en][zh]News & Report[/zh]')?></span>
                </h1>
            </section><!-- ./p-recruit--banner -->
            <div class="p-news">
                <div class="p-news--cnt">
                    <div class="p-news--detail">
                        <?php
                        if (have_posts()): while (have_posts()) : the_post();
                            $news_cat = get_the_terms(get_the_ID(), 'category_news');
                            $main_cat = get_post_meta( get_the_ID(), 'rank_math_primary_category_news', true );
                            ?>
                                <div class="p-news--detail-dateWrap align-center">
                                    <p class="date2 mgb-10"><?php the_time('Y.m.d');?></p>
                                    <?php 
                                    if ( $news_cat && ! is_wp_error( $news_cat ) ) :
                                        foreach ($news_cat as $news_cat_data):
                                            if($news_cat_data->term_id == $main_cat) {
                                                printf('<span class="label2">%s</span>', $news_cat_data->name);
                                            }else {
                                                printf('<span class="label2">%s</span>', $news_cat_data->name);
                                            }
                                            break;
                                        endforeach;
                                    endif;?>                            
                                </div>
                                <div class="p-news--detail-cnt no-reset">
                                    <h2><?php the_title()?></h2>
                                    <?php if ( has_post_thumbnail() ) : ?>
                                        <div class="thumb-center">
                                            <?php the_post_thumbnail('large', array('class' => '')); ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php
                                    if (have_rows('layout_news')):
                                        while (have_rows('layout_news')) : the_row();
                                            switch (get_row_layout()):
                                                case 'l_image':?>
                                                    <div class="mgb-50 w-100"><img src="<?php the_sub_field('image')?>"></div>
                                                <?php
                                                break;
                                                case 'l_2image':
                                                    $col2_img1 = get_sub_field('image1');
                                                    $col2_img2 = get_sub_field('image2');
                                                    ?>
                                                    <div class="col2">
                                                        <div class="col2-item">
                                                            <div class="thumb">
                                                                <img src="<?php echo $col2_img1['sizes']['medium_large']?>" alt="">
                                                            </div>
                                                            <p class="thumb-ttl"><?php echo esc_html($col2_img1['caption']); ?></p>
                                                        </div>
                                                        <div class="col2-item">
                                                            <div class="thumb">
                                                                <img src="<?php echo $col2_img2['sizes']['medium_large']?>" alt="">
                                                            </div>
                                                            <p class="thumb-ttl"><?php echo esc_html($col2_img2['caption']); ?></p>
                                                        </div>
                                                    </div>
                                                <?php
                                                break;
                                                case 'l_heading':?>
                                                    <h3><?php the_sub_field('title')?></h3>
                                                <?php
                                                break;
                                                case 'l_content_leftimg':?>
                                                    <div class="col2-55">
                                                        <div class="col2-55--left">
                                                            <h4><?php the_sub_field('title') ?></h4>
                                                            <p><?php echo nl2br(get_sub_field('content')) ?></p>
                                                        </div>
                                                        <div class="col2-55--right">
                                                            <div class="thumb">
                                                                <img src="<?php the_sub_field('image') ?>" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php
                                                break;
                                                case 'l_content_rightimg':?>
                                                    <div class="col2-45">
                                                        <div class="col2-45--left">
                                                            <div class="thumb">
                                                                <img src="<?php the_sub_field('image') ?>" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col2-45--right">
                                                            <h4><?php the_sub_field('title') ?></h4>
                                                            <p><?php echo nl2br(get_sub_field('content')) ?></p>
                                                        </div>
                                                    </div>
                                                <?php
                                                break;
                                                case 'l_content':?>
                                                    <div class="l-content">
                                                        <?php the_sub_field('content')?>
                                                    </div><!--/pictRowArea-->
                                                <?php
                                                break;
                                                case 'l_youtube':?>
                                                    <div class="iframe">
                                                        <?php the_sub_field('yembed')?>
                                                        <p class="iframe-ttl"><?php the_sub_field('caption')?></p>
                                                    </div>
                                                <?php
                                                break;
                                                case 'l_link':
                                                    $link_type = get_sub_field('type');
                                                    if($link_type === 'pdf') {?>
                                                        <div><a href="<?php the_sub_field('file')?>" class="link-icon pdf" target="_blank"><?php the_sub_field('title')?></a></div>
                                                    <?php
                                                    } elseif($link_type === 'blank') {?>
                                                        <div><a href="<?php the_sub_field('target')?>" class="link-icon blank" target="_blank"><?php the_sub_field('title')?></a></div>
                                                    <?php
                                                    } elseif($link_type === 'self') {?>
                                                        <div><a href="<?php the_sub_field('target')?>" class="link-icon arrow"><?php the_sub_field('title')?></a></div>
                                                    <?php }?>
                                                <?php
                                                break;
                                                case 'l_list':
                                                    if (have_rows('items')) {
                                                        echo '<div class="box__border">';
                                                        echo '<ul>';
                                                        while (have_rows('items')) : the_row();
                                                            printf('<li>%s</li>', get_sub_field('title'));
                                                        endwhile;
                                                        echo '</ul>';
                                                        echo '</div>';
                                                    }
                                                break;
                                            endswitch;
                                        endwhile;
                                    endif;?>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        
                        <div class="align-center mgt-70">
                            <?php printf("<a href='%s' class='viewmore2'>".do_shortcode('[ja]一覧へ戻る[/ja][en]Back to List[/en][zh]Back to List[/zh]')."</a>", home_url('news'));?>
                        </div>
                    </div><!-- /.p-news--detail -->
                </div><!-- /.p-news--cnt -->
                <div class="p-news--sidebar">
                    <p class="title-bold mgb-20"><?php echo do_shortcode('[ja]カテゴリー[/ja][en]Category[/en][zh]Category[/zh]')?></p>
                    <ul class="p-news--sidebar-list">
                        <?php
                        $news_terms = get_terms('category_news');
                        foreach ($news_terms as $news_term) {
                            $term_link = get_term_link($news_term, 'category_news');?>
                        
                            <li class="link"><a href="<?php echo $term_link;?>"><?php echo $news_term->name;?></a></li>
                        <?php } ?>                        
                    </ul>
                    <p class="title-bold mgb-20"><?php echo do_shortcode('[ja]過去のお知らせ[/ja][en]Archive[/en][zh]Archive[/zh]')?></p>
                    <select name="year" class="select" onchange="document.location.href='/news?y='+this.options[this.selectedIndex].value;">
                        <?php
                        printf('<option value="">'.do_shortcode('[ja]掲載年で絞り込む[/ja][en]Please select[/en][zh]Please select[/zh]').'</option>');
                        ?>
                        <?php
                        $start_year = 2019;
                        for ($curr_Y = date('Y'); $curr_Y >= $start_year; $curr_Y--) :
                            ?>
                            <option value="<?php echo $curr_Y ?>"><?php echo $curr_Y ?></option>

                        <?php endfor; ?>                        
                    </select>
                </div><!-- .p-news--sidebar -->
            </div><!-- ./p-news -->
        </div>
    </div>
    <div class="align-center mgt-60">
        <?php 
        printf("<a href='%s' class='viewmore2'>".do_shortcode('[ja]トップページへ戻る[/ja][en]Back to Top[/en][zh]Back to Top[/zh]')."</a>", home_url());
        ?>
    </div>
</main>

<?php get_footer(); ?>

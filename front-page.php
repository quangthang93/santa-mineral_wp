<?php get_header(); ?>
<?php 
if (have_posts()): while (have_posts()) : the_post();
    $top_about = get_field('top_about');
    $top_tech = get_field('top_tech');
    ?>
<main class="main p-top">
    <section class="mv">
        <div class="mv-ttl--wrap">
            <div class="mv-ttl--inner">
                <h2 class="mv-ttl">
                    <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/assets/images/common/title-mv.svg" alt="Santa Mineral （サンタミネラル）">
                    <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/assets/images/common/title-mv-sp.svg" alt="Santa Mineral （サンタミネラル）">
                </h2>
                <div class="mv-cloud sp-only2 fadeleft delay-20 js-mvCloud">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/cloud.png" alt="">
                </div>
            </div>
        </div><!-- ./mv-ttl--wrap -->
        <div class="mv-bgWrap">
            <div class="mv-magicWrap">
                <div class="mv-magic island fuwafuwa">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_island.svg" alt="">
                </div>
                <div class="mv-magic cloud3 fuwafuwa-cloud">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_cloud3.svg" alt="">
                </div>
                <div class="mv-magic cloud8 fuwafuwa-cloud">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_cloud8.svg" alt="">
                </div>
                <div class="mv-magic cloud10 fuwafuwa-cloud">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_cloud10.svg" alt="">
                </div>
                <div class="mv-magic cloud11 fuwafuwa-cloud">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_cloud11.svg" alt="">
                </div>
                <div class="mv-magic dog pikopiko2">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_dog.svg" alt="">
                </div>
                <div class="mv-magic balloon fuwafuwa2">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/amine_balloon.svg" alt="">
                </div>
                <div class="mv-magic woman pikopiko2">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_woman.svg" alt="">
                </div>
                <div class="mv-magic fish fuwafuwa">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_fish.svg" alt="">
                </div>
                <div class="mv-magic girl pikopiko">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_girl.svg" alt="">
                </div>
                <div class="mv-magic boy pikopiko">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_boy.svg" alt="">
                </div>
                <div class="mv-magic bird fuwafuwa2">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_bird.svg" alt="">
                </div>
                <div class="mv-magic bear fuwafuwa3">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_bear.svg" alt="">
                </div>
                <div class="mv-magic cloud9 fuwafuwa-cloud">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_cloud9.svg" alt="">
                </div>
                <div class="mv-magic bird7 fuwafuwa2">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_bird7.svg" alt="">
                </div>
                <div class="mv-magic cloud12 fuwafuwa-cloud">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_cloud12.svg" alt="">
                </div>
                <div class="mv-magic cloud13 fuwafuwa-cloud">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/anime_cloud13.svg" alt="">
                </div>
            </div>
        </div>
        <div class="scroll js-scroll">
            <a href="javascript:void(0)">PLEASE SCROLL</a>
        </div>
    </section><!-- ./mv -->

    <div class="p-top--intro">
        <div class="p-top--intro-cnt">
            <div class="p-top--intro-box1">
                <div class="ani-border">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <p class="p-top--intro-box1-infor"><?php echo $top_about['title']?></p>
            </div>
            <div class="p-top--intro-box2Wrap">
                <div class="p-top--intro-box2">
                    <div class="ani-border">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <p class="p-top--intro-box2-infor"><?php echo nl2br($top_about['content'])?></p>
                </div>
                <div class="p-top--intro-box2Wrap-direct">
                    <?php 
                    if($locale == 'ja') {
                        printf('<a class="link-pink faderight" href="%s"><span>%s</span></a>', home_url('/about'), '当社について');
                    } elseif ($locale == 'en_US') {
                        printf('<a class="link-pink faderight" href="%s"><span>%s</span></a>', home_url('/about'), 'About Us');
                    } elseif ($locale == 'zh_CN') {
                        printf('<a class="link-pink faderight" href="%s"><span>%s</span></a>', home_url('/about'), '關於我們');
                    }
                    ?>
                </div>
            </div>
        </div>
    </div><!-- ./p-top--intro -->

    <section class="p-top--technology fadeup">
        <div class="p-top--technology-cnt">
            <div class="p-top--technology-cnt-infor">
                <div class="title-boxWrap">
                    <div class="ani-border type2">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <h3 class="title-box type2"><?php echo $top_tech['title']?></h3>
                </div>
                <p class="desc"><?php echo nl2br($top_tech['content'])?></p>
            </div>
            <div class="p-top--technology-cnt-img">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/icon-water.svg" alt="">
            </div>
        </div><!-- ./p-top--technology-cnt -->
        <div class="p-top--technology-direct">
            <?php 
            if($locale == 'ja') {
                printf('<a class="link-pink faderight" href="%s"><span>%s</span></a>', home_url('/technology'), 'QQテクノロジーとは');
            } elseif ($locale == 'en_US') {
                printf('<a class="link-pink faderight" href="%s"><span>%s</span></a>', home_url('/technology'), 'What is QQ Technology');
            } elseif ($locale == 'zh_CN') {
                printf('<a class="link-pink faderight" href="%s"><span>%s</span></a>', home_url('/technology'), 'QQ Technology是什麼');
            }
            ?>
        </div><!-- ./p-top--technology-direct -->
    </section><!-- ./p-top--technology -->

    <section class="p-top--service">
        <?php the_field('top_product')?>
    </section><!-- ./p-top--service -->

    <section class="p-top--news fadeup">
        <div class="p-top--news-inner">
            <h3 class="title-quad">NEWS / REPORTS</h3>
            <ul class="p-top--news-list js-newsSliders">
                <?php
                $numpost = get_field('top_news');
                $args['post_type'] = 'news';
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $args['paged'] = $paged;
                $args['post_status'] = 'publish';
                $args['posts_per_page'] = $numpost;
                $args['orderby'] = 'date';
                $args['order'] = 'DESC';
                
                $news_query = null;
                $news_query = new WP_Query($args);
                if ($news_query->have_posts()): while ($news_query->have_posts()) : $news_query->the_post();
                    $news_cat = get_the_terms(get_the_ID(), 'category_news');                            
                    $main_cat = get_post_meta( get_the_ID(), 'rank_math_primary_category_news', true );
                    ?>

                    <li class="p-top--news-item">
                        <a href="<?php the_permalink()?>" class="link">
                            <div class="p-top--news-item-thumbWrap">
                                <?php 
                                if ( $news_cat && ! is_wp_error( $news_cat ) ) :
                                    foreach ($news_cat as $news_cat_data):
                                        if($news_cat_data->term_id == $main_cat) {
                                            printf('<span class="label type2">%s</span>', $news_cat_data->name);
                                        }else {
                                            printf('<span class="label type2">%s</span>', $news_cat_data->name);
                                        }
                                        break;
                                    endforeach;
                                endif;?>                                        
                                <div class="p-top--news-item-thumb">
                                    <?php if ( has_post_thumbnail() ) : ?>
                                        <?php the_post_thumbnail('medium', array('class' => 'cover')); ?>
                                    <?php else:?>
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/no-image.png" class="cover" />
                                    <?php endif; ?>
                                </div>
                                <div class="p-top--news-item-cnt">
                                    <p class="date"><?php the_time('Y.m.d');?></p>
                                    <p class="p-top--news-item-ttl"><?php echo cut_title(get_the_title(), 34)?></p>
                                </div>
                            </div>
                        </a>
                    </li>                                                                   
                    <?php endwhile; wp_reset_postdata();?>
                <?php endif; ?>                
            </ul>
            <div class="viewmoreWap">
                <?php 
                if($locale == 'ja') {
                    printf("<a href='%s' class='viewmore'>%s</a>", home_url('news'), '一覧を見る');
                } elseif ($locale == 'en_US') {
                    printf("<a href='%s' class='viewmore'>%s</a>", home_url('news'), 'More');
                } elseif ($locale == 'zh_CN') {
                    printf("<a href='%s' class='viewmore'>%s</a>", home_url('news'), 'More');
                }
                ?>
            </div>
        </div>
    </section><!-- ./p-top--news -->

</main><!-- ./main -->

        <?php the_content(); ?>		
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>

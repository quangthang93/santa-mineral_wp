<?php get_header(); ?>

<main class="main p-end">
    <div class="p-end--cnt">
        <div class="container">
            <div class="breadcrumbWrap">
                <div class="breadcrumb">
                    <?php wp_breadcrumb() ?>
                </div>
            </div><!-- ./breadcrumbWrap -->
            <section class="p-end--banner type2">
                <h1 class="p-end--ttl">
                    <img src="<?php the_field('title_en'); ?>" alt="">
                    <span><?php the_title() ?></span>
                </h1>
            </section><!-- ./p-recruit--banner -->
            <div class="p-about">
                <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                    <div class="p-top--intro">
                        <div class="p-top--intro-cnt">
                            <div class="p-top--intro-box1">
                                <div class="ani-border">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                                <p class="p-top--intro-box1-infor">
                                    <?php echo nl2br(get_field('about_title'))?>
                                </p>
                            </div>
                            <div class="p-top--intro-box2Wrap">
                                <div class="p-top--intro-box2">
                                    <div class="ani-border">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                    <div class="p-top--intro-box2-infor">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>

                        <?php endwhile; ?>
                        </div>
                    </div>
                    <div class="p-about--cnt">
                        <?php 
                        if($locale == 'ja') {
                            printf('<h2 class="title-lv2">%s</h2>', '当社の強み');
                        } elseif ($locale == 'en_US') {
                            printf('<h2 class="title-lv2">%s</h2>', 'Our strengths');
                        } elseif ($locale == 'zh_CN') {
                            printf('<h2 class="title-lv2">%s</h2>', '我們的優勢');
                        }
                        ?>
                        <div class="p-about--cnt-list">
                            <?php
                            if( have_rows('strengths') ):                                
                                while( have_rows('strengths') ) : the_row();
                                    $btn_link = get_sub_field('grp_link');
                                ?>
                            
                                    <div class="p-about--cnt-item">
                                        <h3 class="title-lv3"><?php the_sub_field('title')?></h3>
                                        <div class="p-about--cnt-item-inner">
                                            <div class="p-about--cnt-item-des desc2">
                                                <?php echo nl2br(get_sub_field('content'))?>
                                                <?php if($btn_link['title'] !== "" && $btn_link['target'] !== "") {?>
                                                    <div><a href="<?php echo $btn_link['target']?>" class="link-border"><span><?php echo $btn_link['title']?></span></a></div>
                                                <?php }?>
                                            </div>
                                            <div class="p-about--cnt-item-thumb">
                                                <img class="cover" src="<?php the_sub_field('image')?>">
                                            </div>
                                        </div>
                                    </div><!-- ./p-about--cnt-item -->
                                <?php
                                endwhile;
                            endif;
                            ?>
                        </div><!-- ./p-about--cnt-list -->
                    </div>
                <?php endif; ?>
            </div><!-- ./p-about -->
        </div>
    </div>    
    
    <div class="align-center mgt-60">
        <?php 
        if($locale == 'ja') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url(), 'トップページへ戻る');
        } elseif ($locale == 'en_US') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url(), 'Back to Top');
        } elseif ($locale == 'zh_CN') {
            printf("<a href='%s' class='viewmore2'>%s</a>", home_url(), 'Back to Top');
        }
        ?>
    </div>
</main>

<?php get_footer(); ?>
